package com.fastpay.app.fastpay.administrator.transfer;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.FormatterText;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by skyshi on 28/01/17.
 */

public class TransferFragment extends Fragment implements View.OnClickListener{
    private static final String TAG="TransferFragment";
    private EditText edit_id_tujuan,edit_nominal_transfer,edit_pin,edit_berita;
    private Button button_cek;
    private ProgressBar loading;
    private Dialog dialog;
    private StringJson stringJson;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stringJson = new StringJson(getActivity());
        loading = (ProgressBar)view.findViewById(R.id.loadingView);
        button_cek = (Button)view.findViewById(R.id.button_transfer_cek);
        button_cek.setOnClickListener(this);
        edit_id_tujuan =(EditText)view.findViewById(R.id.edit_transfer_id);
        edit_nominal_transfer =(EditText)view.findViewById(R.id.edit_transfer_nominal);
        edit_pin =(EditText)view.findViewById(R.id.edit_transfer_password);
        edit_berita =(EditText)view.findViewById(R.id.edit_transfer_berita);
    }

    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createTransferListener(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private com.android.volley.Response.Listener<JSONObject> createTransferListener(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {

                        String value = response.getString("response_desc");
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.ic_check_success,R.layout.dialog_layout,getString(R.string.string_transfer),response.getString("response_desc"),"OK");
                        edit_pin.setText("");
                        edit_id_tujuan.setText("");
                        edit_nominal_transfer.setText("");
                        edit_berita.setText("");
                        //webview_komisi.loadData(value,"text/html", "UTF-8");
                    }else{
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc"),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }

    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else if(layoutType == R.layout.dialog_yes_no){
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView texDialogTitle = (TextView)dialog.findViewById(R.id.dialog_title);
            texDialogTitle.setText(title);

            TextView textDialog = (TextView)dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button buttonBatal = (Button) dialog.findViewById(R.id.dialog_button_batal);
            buttonBatal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            Button buttonYa = (Button) dialog.findViewById(R.id.dialog_button_ya);
            buttonYa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String json = stringJson.requestTransfer(edit_id_tujuan.getText().toString(),edit_nominal_transfer.getText().toString(),edit_pin.getText().toString());
                    POSTrequest(json);
                    dialog.dismiss();
                }
            });

        }
        dialog.show();
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.button_transfer_cek){
            if(edit_id_tujuan.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),"ID Tujuan harus Diisi","OK");
            }else if(edit_pin.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),"Pin harus diisi","OK");
            }else if(edit_nominal_transfer.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),"Nominal transfer harus diisi","OK");
            }else {
                setDialog(R.drawable.ic_help_yellow_600_18dp,R.layout.dialog_yes_no, "KONFIRMASI TRANSFER",
                        getString(R.string.string_konfirmasi_transfer) + " ID : " + edit_id_tujuan.getText().toString() + ",dengan nominal : " + FormatterText.CurencyIDR(edit_nominal_transfer.getText().toString() + ""), "");
            }
        }
    }
}
