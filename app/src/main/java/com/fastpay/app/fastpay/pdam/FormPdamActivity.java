package com.fastpay.app.fastpay.pdam;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.MenuCategoryAdapter;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FormPdamActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "FormPdamActivity";
    private EditText id,no_meter;
    private Button button_cek;
    private WebView webview;
    private String code,title;
    private ProgressBar loading;
    private String reff_id_inq,nominal_inq,nominal_admin_inq;
    private StringJson stringJson;
    private Dialog dialog;
    private Toolbar toolbar;
    private InputMethodManager imm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_pdam);
        stringJson = new StringJson(this);
        imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        loading = (ProgressBar)findViewById(R.id.loadingView);
        webview = (WebView)findViewById(R.id.webview_pdam);
        webview.getSettings().setJavaScriptEnabled(true);
        Intent i = getIntent();
        code = i.getStringExtra("code");
        title = i.getStringExtra("title");
        id = (EditText)findViewById(R.id.edit_pdam_id);
        no_meter = (EditText)findViewById(R.id.edit_pdam_nometer);
        button_cek = (Button)findViewById(R.id.button_pdam_check);
        button_cek.setOnClickListener(this);
        setToolbar(title);
    }
    public void setToolbar(String title){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                finish();
        }
        return true;
    }
    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createInquiryPDAm(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private com.android.volley.Response.Listener<JSONObject> createInquiryPDAm(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                       String value = response.getString("response_desc");
                        reff_id_inq = response.getString("reff_id_inq");
                        nominal_admin_inq = response.getString("nominal_admin_inq");
                        nominal_inq = response.getString("nominal_inq");
                        //webview.loadData(value,"text/html", "UTF-8");
                        loading.setVisibility(View.GONE);
                        Intent a = new Intent(FormPdamActivity.this,InquiryPDAMActivity.class);
                        a.putExtra("id_inq",reff_id_inq);
                        a.putExtra("admin_inq",nominal_admin_inq);
                        a.putExtra("nominal_inq",nominal_inq);
                        a.putExtra("value",value);
                        a.putExtra("code",code);
                        a.putExtra("title",title);
                        a.putExtra("code_pelanggan",id.getText().toString());
                        a.putExtra("nometer",no_meter.getText().toString());
                        startActivity(a);
                        id.setText("");
                        no_meter.setText("");
                    }else{
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }
    public void setDialog(int icon,int layoutType,int title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }
    @Override
    public void onClick(View view) {
        if(id.getText().toString().equalsIgnoreCase("")&&no_meter.getText().toString().equalsIgnoreCase("")){
            setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Masukkan ID Pelanggan atau NO Meter",0,"OK");
        }else {
            String json = stringJson.requestInquiryPDAM(code, id.getText().toString(), no_meter.getText().toString());
            POSTrequest(json);
        }
    }
}
