package com.fastpay.app.fastpay.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.fastpay.app.fastpay.R;

import java.util.ArrayList;

/**
 * Created by skyshi on 29/01/17.
 */

public class MenuListPropinsiAdapter extends ArrayAdapter<String> implements Filterable{
    ArrayList<String> titles = new ArrayList<>();
    int resource;
    public MenuListPropinsiAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, 0, objects);
        this.titles = objects;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource,parent,false);
        }
        TextView textTitleMenu = (TextView)convertView.findViewById(R.id.title_menu);
        textTitleMenu.setText(titles.get(position));
        return convertView;
    }
}
