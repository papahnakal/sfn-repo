package com.fastpay.app.fastpay.laporantransaksi;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.FormatterText;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.WebViewActivity;
import com.fastpay.app.fastpay.adapter.CetakLaporanAdapter;
import com.fastpay.app.fastpay.adapter.MenuPopUpAdapter;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.kartukredit.KartuKreditInquiryActivity;
import com.fastpay.app.fastpay.kartukredit.KartukreditFormActivity;
import com.fastpay.app.fastpay.object.CetakLaporanObj;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class LaporanTransaksiActivity extends Fragment implements View.OnClickListener,DatePickerDialog.OnDateSetListener{
    private static final String TAG = "laporanTrans";
    DatePickerDialog datePickerDialog;
    EditText edit_date,no_pelanggan;
    Button buttonCariData;
    String dateLaporan = "",dateYesterday="";
    Dialog dialog;
    StringJson stringJson;
    ProgressBar loadingView;
    WebView webview_laporan;
    ListView listLaporan;
    CetakLaporanAdapter adapter;
    private CetakLaporanObj cetakLaporanObj;
    private ArrayList<CetakLaporanObj> cetakArray ;
    TextView text_pilih_semua,text_bersihkan,text_print_terpilih,text_print_semua;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_laporan_transaksi,null);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadingView = (ProgressBar)view.findViewById(R.id.loadingView);
        edit_date = (EditText)view.findViewById(R.id.edit_time_pick);
        edit_date.setOnClickListener(this);
        no_pelanggan = (EditText)view.findViewById(R.id.no_pelanggan);
        buttonCariData = (Button)view.findViewById(R.id.button_cari_Data);
        buttonCariData.setOnClickListener(this);
        webview_laporan = (WebView)view.findViewById(R.id.webview_laporan);
        webview_laporan.getSettings().setJavaScriptEnabled(true);
        text_pilih_semua = (TextView)view.findViewById(R.id.text_pilih_semua);
        text_pilih_semua.setOnClickListener(this);
        text_bersihkan = (TextView)view.findViewById(R.id.text_bersihkan);
        text_bersihkan.setOnClickListener(this);
        text_print_terpilih = (TextView)view.findViewById(R.id.text_print_terpilih);
        text_print_terpilih.setOnClickListener(this);
        text_print_semua = (TextView)view.findViewById(R.id.text_print_semua);
        text_print_semua.setOnClickListener(this);
        listLaporan = (ListView)view.findViewById(R.id.list_cetak_laporan);

        stringJson = new StringJson(getActivity());
        long timeNow = Calendar.getInstance().getTimeInMillis();
        //Calendar cal = Calendar.getInstance();
        //cal.add(Calendar.DATE, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Log.d(TAG,"hint : "+sdf.format(timeNow));
        //Log.d(TAG,"hint : "+sdf.format(cal.getTime()));
        //edit_date.setHint(sdf.format(timeNow));
        edit_date.setText(sdf.format(timeNow));
        dateLaporan = sdf.format((timeNow));
        //dateYesterday = sdf.format(cal.getTime());
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        String date =FormatterText.leadZero(dayOfMonth)  + "-" + FormatterText.leadZero(month + 1) + "-" + year;
        String dateSend =year  + "-" + FormatterText.leadZero(month + 1) + "-" + FormatterText.leadZero(dayOfMonth);
        dateYesterday = dateSend;
        edit_date.setText(date);
    }
    public void POSTrequest(String json,String type){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loadingView.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = null;
            if(type.equalsIgnoreCase("cetak")){
                post = new JsonObjectRequest(Request.Method.POST,Config.BASE_URL,jsonBody,cetakLaporan(),createMyReqErrorListener());
            }else {
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createLaporan(), createMyReqErrorListener());
            }
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private com.android.volley.Response.Listener<JSONObject>cetakLaporan(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        String html = response.getString("response_desc");
                        String struk_tercetak = response.getString("struk_tercetak");
                        //Log.d(TAG,response.getString("response_value"));
                        Intent a = new Intent(getActivity(),CUlaporan.class);
                        a.putExtra("value",html);
                        a.putExtra("struk_tercetak",struk_tercetak);
                        startActivity(a);
                        loadingView.setVisibility(View.GONE);
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                        loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.Listener<JSONObject> createLaporan(){
        cetakArray = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        //loadingView.setVisibility(View.GONE);
                        //String value = response.getString("response_value");
                        JSONArray list = response.getJSONArray("response_value");
                        //if(list.length()!=0) {
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject transaksi = (JSONObject) list.get(i);
                                String id_transaksi = transaksi.getString("id_transaksi");
                                String keterangan = transaksi.getString("keterangan");
                                String status_transaksi = transaksi.getString("status_transaksi");
                                if(!status_transaksi.toString().equalsIgnoreCase("gagal")) {
                                    cetakLaporanObj = new CetakLaporanObj(id_transaksi, keterangan, status_transaksi);
                                    cetakArray.add(cetakLaporanObj);
                                }
                                Log.d(TAG, id_transaksi + "||" + keterangan + "||" + status_transaksi);
                            }
                            adapter = new CetakLaporanAdapter(getActivity(), R.layout.list_cetak_laporan, cetakArray, new CetakLaporanAdapter.ListenerList() {
                                @Override
                                public void onClickCetak(CetakLaporanObj obj) {
                                    Log.d(TAG, obj.getId_transaksi());
                                    String json = stringJson.requestCetakTransaksi(obj.getId_transaksi());
                                    Log.d(TAG, json);
                                    POSTrequest(json, "cetak");
                                }
                            });
                            listLaporan.setAdapter(adapter);
                            //Intent a = new Intent(getActivity(),WebViewActivity.class);
                            //a.putExtra("value",value);
                            //startActivity(a);
                            no_pelanggan.setText("");
                            //edit_date.setText("");
                            loadingView.setVisibility(View.GONE);
                        /*}else{
                            setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Maaf, tidak ada transaksi sebelumnya.",0,"OK");
                        }*/
                        //webview_laporan.loadData(value,"text/html", "UTF-8");
                    }else{
                        loadingView.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                loadingView.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }
    public void setDialog(int icon,int layoutType,int title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.edit_time_pick){
            Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            System.out.println("the selected " + mDay);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                    this, mYear, mMonth, mDay);
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            dialog.show();
        }else if(id == R.id.button_cari_Data){
            if(!edit_date.getText().toString().equalsIgnoreCase("")) {
                String json = stringJson.requestLaporanTransaksi(dateYesterday, dateLaporan, no_pelanggan.getText().toString());
                Log.d(TAG,json);
                POSTrequest(json,"");
            }else{
                setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Masukkan tanggal",0,"OK");
            }
        }else if(id == R.id.text_print_semua){

        }else if(id == R.id.text_print_terpilih){

        }else if(id == R.id.text_bersihkan){
            //webview_laporan.clearView();
        }else if(id == R.id.text_pilih_semua){

        }
    }

}
