package com.fastpay.app.fastpay.administrator.cekpulsa;

import android.app.Dialog;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.pln.FormCekIdPlnActivity;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CheckHargaPulsaFragment extends Fragment implements AdapterView.OnItemSelectedListener,View.OnClickListener{
    private static final String TAG = "fragmentChecHargaPulsa";
    private Spinner spinner_operator;
    private Button button_kirim;
    private WebView webviewCheckpulsa;
    private StringJson stringJson;
    private ArrayList<String> listOperatorCode;
    private Dialog dialog;
    private ProgressBar loading;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_check_pulsa,container,false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stringJson = new StringJson(getActivity());
        spinner_operator = (Spinner)view.findViewById(R.id.spinner_list_operator_seluler);
        spinner_operator.setOnItemSelectedListener(this);
        webviewCheckpulsa = (WebView)view.findViewById(R.id.webview_checkpulsa);
        webviewCheckpulsa.getSettings().setJavaScriptEnabled(true);
        button_kirim = (Button)view.findViewById(R.id.button_checkpulsa_kirim);
        button_kirim.setOnClickListener(this);
        loading = (ProgressBar)view.findViewById(R.id.loadingView);
        String json = stringJson.requestOperatorSel();
        POSTrequest(json,Config.MENU);
    }
    public void POSTrequest(String json,String type){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = null;
            if(type.equalsIgnoreCase(Config.MENU)){
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createOperatorListener(), createMyReqErrorListener());
            }else if(type.equalsIgnoreCase(Config.CHECKPULSA)){
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createCheckPulsaListener(), createMyReqErrorListener());
            }
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private com.android.volley.Response.Listener<JSONObject> createOperatorListener(){
        final ArrayList<String> listOperatorName = new ArrayList<>();
        listOperatorCode = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        JSONArray array = response.getJSONArray("response_value");
                        listOperatorName.add("Pilih Nama Operator");
                        listOperatorCode.add("Pilih Nama Operator");
                        for (int i = 0; i <array.length() ; i++) {
                            JSONObject opsel = (JSONObject) array.get(i);
                            String opsel_name = opsel.getString("opsel_name");
                            String opsel_code = opsel.getString("opsel_code");
                            listOperatorName.add(opsel_name);
                            listOperatorCode.add(opsel_code);

                        }
                        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<>(getActivity(),R.layout.spinner_single_text,listOperatorName);
                        spinner_operator.setAdapter(adapterSpinner);
                        spinner_operator.setSelection(0);
                        Log.d(TAG, "ulala : " + response.getString("response_code"));
                        loading.setVisibility(View.GONE);
                    }else{
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }
    private com.android.volley.Response.Listener<JSONObject> createCheckPulsaListener(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        String value = response.getString("response_value");
                        webviewCheckpulsa.loadData(value,"text/html", "UTF-8");
                        loading.setVisibility(View.GONE);
                    }else{
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.button_checkpulsa_kirim){
            if (spinner_operator.getSelectedItemPosition()!=0) {
                String code = listOperatorCode.get(spinner_operator.getSelectedItemPosition());
                Log.d(TAG, "spinner code : " + code);
                String json = stringJson.requestHargaPulsa(code);
                POSTrequest(json, Config.CHECKPULSA);
            }else{
                setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),getString(R.string.string_operator_name_warning),"OK");
            }
        }
    }
    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
}
