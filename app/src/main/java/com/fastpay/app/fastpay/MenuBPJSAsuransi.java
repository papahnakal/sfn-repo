package com.fastpay.app.fastpay;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.fastpay.app.fastpay.adapter.MenuCategoryAdapter;
import com.fastpay.app.fastpay.asuransi.MenuAsuransiActivity;
import com.fastpay.app.fastpay.bpjs.BPJSKSActivity;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.pln.FormCekIdPlnActivity;
import com.fastpay.app.fastpay.pln.MenuPlnActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MenuBPJSAsuransi extends AppCompatActivity implements AdapterView.OnItemClickListener,MenuCategoryAdapter.ListenerList{
    ImageView search;
    ListView listMenu;
    ArrayList<MenuObj> menuPLN = new ArrayList();
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_bpjsasuransi);
        search = (ImageView)findViewById(R.id.search_image);
        listMenu = (ListView)findViewById(R.id.list_menu_pln);
        setToolbar();
        List<String> StringMenu = Arrays.asList(getResources().getStringArray(R.array.asuransi_menu));
        for (int i = 0; i < StringMenu.size(); i++) {
            MenuObj a = new MenuObj(StringMenu.get(i),"");
            menuPLN.add(a);
        }
        listMenu.setAdapter(new MenuCategoryAdapter(this,R.layout.list_category_main_menu,menuPLN, Config.BPJS,this));
        //listMenu.setOnItemClickListener(this);
    }
    public void setToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.string_bpjs_asuransi);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String value = (String)adapterView.getItemAtPosition(i);
        if(value.equalsIgnoreCase(Config.BPJS)){
            //Toast.makeText(this,"1",Toast.LENGTH_SHORT).show();
            Intent pascabayar = new Intent(MenuBPJSAsuransi.this,BPJSKSActivity.class);
            pascabayar.putExtra("code","ASRBPJSKS");
            startActivity(pascabayar);
        }else if(value.equalsIgnoreCase(Config.ASURANSI)){
            //Toast.makeText(this,"2",Toast.LENGTH_SHORT).show();
            Intent prabayar = new Intent(MenuBPJSAsuransi.this,MenuAsuransiActivity.class);
            //prabayar.putExtra("menu","2");
            startActivity(prabayar);
        }
            //Toast.makeText(this,"3",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClickedList(MenuObj obj) {
        String value = obj.getNameMenu();
        if(value.equalsIgnoreCase(Config.BPJS)){
            //Toast.makeText(this,"1",Toast.LENGTH_SHORT).show();
            Intent pascabayar = new Intent(MenuBPJSAsuransi.this,BPJSKSActivity.class);
            pascabayar.putExtra("code","ASRBPJSKS");
            startActivity(pascabayar);
        }else if(value.equalsIgnoreCase(Config.ASURANSI)){
            //Toast.makeText(this,"2",Toast.LENGTH_SHORT).show();
            Intent prabayar = new Intent(MenuBPJSAsuransi.this,MenuAsuransiActivity.class);
            //prabayar.putExtra("menu","2");
            startActivity(prabayar);
        }
    }
}
