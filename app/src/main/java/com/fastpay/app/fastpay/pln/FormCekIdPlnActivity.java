package com.fastpay.app.fastpay.pln;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.MainActivity;
import com.fastpay.app.fastpay.MenuPopUp;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.login.LoginActivity;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.object.SignOn;
import com.fastpay.app.fastpay.request.JSONRequest;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class FormCekIdPlnActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,View.OnClickListener{
    private static String TAG = "FormCheckIDPln";
    private static String MENU = "menu";
    private EditText edit_id;
    private Button button_cek;
    //private Spinner spinner_tokenpln;
    private MenuObj menuList;
    private ArrayList<MenuObj> menuArray ;
    private EditText tokenlistrik;
    private String typeMenu;
    private Dialog dialog;
    private JSONRequest jsonRequest;
    private StringJson stringJson;
    private ArrayAdapter<String> adapterSpinner ;
    private ProgressBar loadingView;
    private LinearLayout lin_pln_form;
    private Toolbar toolbar;
    private String codetoken;
    private TextInputLayout input;
    private InputMethodManager imm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_cek_id_pln);
        jsonRequest = new JSONRequest();
        stringJson = new StringJson(this);
        Intent intent = getIntent();
        typeMenu = intent.getStringExtra("menu");
        Log.d(TAG,typeMenu);
        imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        input = (TextInputLayout)findViewById(R.id.input_pdam_nometer);
        loadingView = (ProgressBar)findViewById(R.id.loadingView);
        edit_id = (EditText)findViewById(R.id.edit_id_pelanggan);
        button_cek = (Button)findViewById(R.id.button_check);
        lin_pln_form = (LinearLayout)findViewById(R.id.lin_pln_form);
        button_cek.setOnClickListener(this);
        tokenlistrik = (EditText)findViewById(R.id.edit_token_listrik);
        tokenlistrik.setOnClickListener(this);
        //spinner_tokenpln = (Spinner)findViewById(R.id.spinner_token_listrik);
        //spinner_tokenpln.setOnItemSelectedListener(this);

        String json = stringJson.requestTokenPrice();
        POSTrequest(json,MENU);
        viewConfig();
    }
    public void setToolbar(String title){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                finish();
        }
        return true;
    }
    public void viewConfig(){
        if(typeMenu.equalsIgnoreCase("1")){
            setToolbar(getString(R.string.string_pln_pascabayar_title));
            input.setHint(getString(R.string.string_id_pelanggan));
            edit_id.setInputType(InputType.TYPE_CLASS_NUMBER);
            tokenlistrik.setVisibility(View.GONE);
            //spinner_tokenpln.setVisibility(View.GONE);
        }else if(typeMenu.equalsIgnoreCase("2")){
            setToolbar(getString(R.string.string_pln_prabayar_title));
            input.setHint(getString(R.string.string_id_nometer));
            //edit_id.setHint(R.string.string_id_nometer);
            tokenlistrik.setVisibility(View.VISIBLE);
            //spinner_tokenpln.setVisibility(View.VISIBLE);
        }else{
            setToolbar(getString(R.string.string_pln_nontalgis_title));
            input.setHint(getString(R.string.string_no_registrasi));
            tokenlistrik.setVisibility(View.GONE);
            //spinner_tokenpln.setVisibility(View.GONE);
        }
    }
    public void POSTrequest(String json,String type){
        Log.d(TAG,type);
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            final JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = null;
            if(type.equalsIgnoreCase(MENU)) {
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createMenuListener(), createMyReqErrorListener());
            }else if(type.equalsIgnoreCase(Config.PASCABAYAR)){
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createPascabayarListener(), createMyReqErrorListener());
            }else if(type.equalsIgnoreCase(Config.PRABAYAR)){
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createPrabayarListener(), createMyReqErrorListener());
            }else if(type.equalsIgnoreCase(Config.NONTALGIS)){
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createNontalgisListener(), createMyReqErrorListener());
            }
            queue.add(post);
            loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private com.android.volley.Response.Listener<JSONObject> createMenuListener(){
        //final ArrayList<String> listTokenPln = new ArrayList<>();
        menuArray = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray array = new JSONArray(response.getString("response_value"));
                    //listTokenPln.add("Pilih Nominal Token");
                    for (int i = 0; i <array.length() ; i++) {
                        //listTokenPln.add("Token PLN "+array.getString(i));
                        //JSONObject bank = (JSONObject) array.get(i);
                        String token_code = array.getString(i);
                        String token_name = array.getString(i);
                        Log.d(TAG, "ulala : " + token_code + "" + token_name);
                        menuList = new MenuObj(token_code,token_name);
                        menuArray.add(menuList);
                    }
                    /*ArrayAdapter<String>adapterSpinner = new ArrayAdapter<>(FormCekIdPlnActivity.this,R.layout.spinner_single_text,listTokenPln);
                    spinner_tokenpln.setAdapter(adapterSpinner);
                    spinner_tokenpln.setSelection(0);*/
                    loadingView.setVisibility(View.GONE);
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.Listener<JSONObject> createPascabayarListener(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        Log.d(TAG,"pasca");
                        String tagihan = response.getString("response_desc");
                        String id_inq = response.getString("reff_id_inq");
                        String nominal_inq = response.getString("nominal_inq");
                        String admin_inq = response.getString("nominal_admin_inq");
                        String id = edit_id.getText().toString();
                        Log.d(TAG,id);
                        Intent pascabayar = new Intent(FormCekIdPlnActivity.this, ListrikPascaBayarActivity.class);
                        pascabayar.putExtra("tagihan", tagihan);
                        pascabayar.putExtra("id", id);
                        pascabayar.putExtra("id_inq", id_inq);
                        pascabayar.putExtra("nominal_inq", nominal_inq);
                        pascabayar.putExtra("admin_inq", admin_inq);
                        pascabayar.putExtra("type", Config.PASCABAYAR);
                        startActivity(pascabayar);
                        loadingView.setVisibility(View.GONE);
                        edit_id.setText("");
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                        loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.Listener<JSONObject> createPrabayarListener(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        Log.d(TAG,"pra");
                        String tagihan = response.getString("response_desc");
                        String id_inq = response.getString("reff_id_inq");
                        String nominal_inq = response.getString("nominal_inq");
                        String admin_inq = response.getString("nominal_admin_inq");
                        String id = edit_id.getText().toString();
                        Log.d(TAG,id);
                        Intent prabayar = new Intent(FormCekIdPlnActivity.this, ListrikPascaBayarActivity.class);
                        prabayar.putExtra("tagihan", tagihan);
                        prabayar.putExtra("id", id);
                        prabayar.putExtra("id_inq", id_inq);
                        prabayar.putExtra("nominal_inq", nominal_inq);
                        prabayar.putExtra("admin_inq", admin_inq);
                        prabayar.putExtra("type", Config.PRABAYAR);
                        startActivity(prabayar);
                        loadingView.setVisibility(View.GONE);
                        edit_id.setText("");
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                        loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }
    private com.android.volley.Response.Listener<JSONObject> createNontalgisListener(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        Log.d(TAG,"non");
                        String tagihan = response.getString("response_desc");
                        String id_inq = response.getString("reff_id_inq");
                        String nominal_inq = response.getString("nominal_inq");
                        String admin_inq = response.getString("nominal_admin_inq");
                        String id = edit_id.getText().toString();
                        Log.d(TAG,id);
                        Intent nontalgis = new Intent(FormCekIdPlnActivity.this, ListrikPascaBayarActivity.class);
                        nontalgis.putExtra("tagihan", tagihan);
                        nontalgis.putExtra("id", id);
                        nontalgis.putExtra("id_inq", id_inq);
                        nontalgis.putExtra("nominal_inq", nominal_inq);
                        nontalgis.putExtra("admin_inq", admin_inq);
                        nontalgis.putExtra("type", Config.NONTALGIS);
                        startActivity(nontalgis);
                        loadingView.setVisibility(View.GONE);
                        edit_id.setText("");
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                        loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                loadingView.setVisibility(View.GONE);
            }
        };
    }

    public void setDialog(int icon,int layoutType,int title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else {
            dialog = new Dialog(this,R.style.AppTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_full_title);
            textDialogTitle.setText(title);

            TextView textDialogContentTitle = (TextView)dialog.findViewById(R.id.dialog_full_title_content);
            textDialogContentTitle.setText(contenttitle);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_full_content);
            textDialog.setText(content);

            ImageView backArrow = (ImageView)dialog.findViewById(R.id.back_arrow_dialog);
            backArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();

        // Showing selected spinner item
        //Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.button_check){
            if(typeMenu.equalsIgnoreCase("1")){
                if(edit_id.getText().toString().equalsIgnoreCase("")){
                    setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"No Planggan Harus Diisi",0,"OK");
                }else {
                    String json = stringJson.requestPLNpascabayar(edit_id.getText().toString());
                    POSTrequest(json, Config.PASCABAYAR);
                    //edit_id.setText("");
                }
                /*Intent pascabayar = new Intent(FormCekIdPlnActivity.this,ListrikPascaBayarActivity.class);
                pascabayar.putExtra("id",edit_id.getText().toString());
                startActivity(pascabayar);*/
            }
            else if(typeMenu.equalsIgnoreCase("2")){
                //String token = spinner_tokenpln.getSelectedItem().toString();
                if(!edit_id.getText().toString().equalsIgnoreCase("")) {
                    if(!tokenlistrik.getText().toString().equalsIgnoreCase("")) {
                        /*String[] spliterd = token.split("\\s+");
                        String json = stringJson.requestPLNPrabayar(edit_id.getText().toString(), spliterd[spliterd.length - 1]);
                        Log.d(TAG, "json : " + json);
                        POSTrequest(json, Config.PRABAYAR);
                        //edit_id.setText("");
                        spinner_tokenpln.setSelection(0);*/
                        String json = stringJson.requestPLNPrabayar(edit_id.getText().toString(), codetoken);
                        Log.d(TAG, "json : " + json);
                        POSTrequest(json, Config.PRABAYAR);
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Pilih Nominal Token ",0,"OK");
                    }
                }else{
                    setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"No ID Pelanggan Harus di Isi",0,"OK");
                }
            }else if(typeMenu.equalsIgnoreCase("3")){
                if(!edit_id.getText().toString().equalsIgnoreCase("")) {
                    String json = stringJson.requestPLNNontalgis(edit_id.getText().toString());
                    POSTrequest(json, Config.NONTALGIS);
                }else{
                    setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"No Registrasi Harus di isi",0,"OK");
                }
            }
        }if(id == R.id.edit_token_listrik){
            Intent A = new Intent(FormCekIdPlnActivity.this,MenuPopUp.class);
            A.putExtra("type",Config.RES_PLN);
            A.putParcelableArrayListExtra("list",menuArray);
            startActivityForResult(A, Config.RES_PLN);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //getActivity();
        //Log.d(TAG, "ulala : " + "onhere"+TAG+data.getStringExtra("name"));
        if(data!=null) {
            if (resultCode == Activity.RESULT_OK) {
                tokenlistrik.setText(data.getStringExtra("name"));
                codetoken = data.getStringExtra("code");
            } else {
                tokenlistrik.setText("");
            }
        }
    }

}
