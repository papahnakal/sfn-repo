package com.fastpay.app.fastpay;

import android.app.Application;
import android.content.Context;

import com.fastpay.app.fastpay.adapter.utils.MyVolley;

/**
 * Created by skyshi on 25/01/17.
 */

public class ApplicationClass extends Application{

    private static Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = this.getApplicationContext();
        MyVolley.init(applicationContext);
    }
}
