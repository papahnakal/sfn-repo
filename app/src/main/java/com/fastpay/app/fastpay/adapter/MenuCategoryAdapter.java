package com.fastpay.app.fastpay.adapter;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.object.MenuObj;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by skyshi on 25/01/17.
 */

public class MenuCategoryAdapter extends BaseAdapter {
    ArrayList<MenuObj>titles = new ArrayList<>();
    ArrayList<MenuObj>tempTitles = new ArrayList<>();
    private Context context;
    private ListenerList listener;
    String type;
    private String strFilterText="";
    int resource;
    public MenuCategoryAdapter(Context context, int resource, ArrayList<MenuObj> objects, String type,ListenerList listener) {
        //super(context, 0, objects);
        for (int i = 0; i <objects.size() ; i++) {
            if(this.titles.size()>0){
                if(!this.titles.contains(objects.get(i))){
                    this.titles.add(objects.get(i));
                    this.tempTitles.add(objects.get(i));
                }
            }else{
                this.titles.add(objects.get(i));
                this.tempTitles.add(objects.get(i));
            }
        }
        this.listener = listener;
        this.context = context;
        this.resource = resource;
        this.type = type;
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolderPulsa viewHolderPulsa;
        final ViewHolder viewHolder;
        if(type.equalsIgnoreCase(Config.PULSA)){
            if(convertView == null){
                convertView = infl.inflate(resource, parent, false);
                viewHolderPulsa = new ViewHolderPulsa();
                viewHolderPulsa.txtName = (TextView)convertView.findViewById(R.id.title_menu);
                viewHolderPulsa.icon = (ImageView)convertView.findViewById(R.id.image_category);
                viewHolderPulsa.linBeli = (LinearLayout)convertView.findViewById(R.id.linear_pulsa_beli);
                convertView.setTag(viewHolderPulsa);
                //convertView = LayoutInflater.from(getContext()).inflate(resource,parent,false);
            }else{
                viewHolderPulsa = (ViewHolderPulsa)convertView.getTag();
            }
            if(!titles.get(position).getNameMenu().isEmpty()){
                viewHolderPulsa.txtName.setText(titles.get(position).getNameMenu());
                viewHolderPulsa.icon.setImageResource(R.drawable.ic_telkom);
            }
            viewHolderPulsa.linBeli.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClickedList(titles.get(position));
                }
            });
        }else {
            if (convertView == null) {
                convertView = infl.inflate(resource, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.txtName = (TextView)convertView.findViewById(R.id.title_menu);
                viewHolder.icon = (ImageView)convertView.findViewById(R.id.image_category);
                viewHolder.rel_list_cat = (RelativeLayout)convertView.findViewById(R.id.rel_list_cat);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder)convertView.getTag();
            }
            if(!titles.get(position).getNameMenu().isEmpty()) {
                viewHolder.txtName.setText(titles.get(position).getNameMenu());
                if (type.equalsIgnoreCase(Config.PLN)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_pln);
                } else if (type.equalsIgnoreCase(Config.BPJS)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_bpjs);
                } else if (type.equalsIgnoreCase(Config.ASURANSI)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_bpjs);
                } else if (type.equalsIgnoreCase(Config.PDAM)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_pdam);
                } else if (type.equalsIgnoreCase(Config.TELKOM)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_telkom);
                } else if (type.equalsIgnoreCase(Config.GAME)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_game_11);
                } else if (type.equalsIgnoreCase(Config.HPPASCABAYAR)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_pascabayar);
                } else if (type.equalsIgnoreCase(Config.CICILAN)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_cicilan_pinjaman);
                } else if (type.equalsIgnoreCase(Config.KREDIT)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_kartu_kredit);
                } else if (type.equalsIgnoreCase(Config.TV_KABEL)) {
                    viewHolder.icon.setImageResource(R.drawable.ic_tvkabel);
                }
            }
            viewHolder.rel_list_cat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClickedList(titles.get(position));
                }
            });
        }
        return convertView;
    }
    public interface ListenerList{
        public void onClickedList(MenuObj obj);
    }
    private static class ViewHolderPulsa{
        private TextView txtName;
        private ImageView icon;
        private LinearLayout linBeli;
    }
    private static class ViewHolder{
        private TextView txtName;
        private ImageView icon;
        private RelativeLayout rel_list_cat;
    }
    public void filter(String charText) {
        charText   = charText.toLowerCase(Locale.getDefault());
        strFilterText = charText;
        titles.clear();
        if (charText.length() == 0||charText.equalsIgnoreCase("")) {
            titles.addAll(tempTitles);
        } else {
            /*for (Brands wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    arrayBrandslist.add(wp);
                }
            }*/
            for (int i = 0; i <tempTitles.size() ; i++) {
                if(tempTitles.get(i).getNameMenu().toLowerCase(Locale.getDefault()).contains(charText)){
                    titles.add(tempTitles.get(i));
                }
            }
            /*if (phoneList.size()==0) {
                Brands brands = new Brands(context.getResources().getString(R.string.no_brands_found), "0", "nobrandsfound");
                arrayBrandslist.add(brands);
            }*/
        }
        notifyDataSetChanged();
    }
}
