package com.fastpay.app.fastpay.bpjs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.FormatterText;
import com.fastpay.app.fastpay.Print;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FormBpjsActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "tvKabel";
    private String value,id_inq,nominal_inq,admin_inq;
    private Dialog dialog;
    private Toolbar toolbar;
    private ProgressBar loadingView;
    private WebView webView;
    private Button button_bayar;
    private String code,nomor_va,month_value,struk_tercetak;
    private int total;
    private StringJson stringJson;
    private Print print;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_bpjs);
        Intent intent = getIntent();
        code = intent.getStringExtra("code");
        nomor_va = intent.getStringExtra("nomor_va");
        month_value = intent.getStringExtra("month_value");
        //id_edit = intent.getStringExtra("id");
        value = intent.getStringExtra("value");
        id_inq = intent.getStringExtra("id_inq");
        nominal_inq = intent.getStringExtra("nominal_inq");
        admin_inq = intent.getStringExtra("admin_inq");
        if(id_inq.equalsIgnoreCase("")){
            id_inq = "0";
        }if(nominal_inq.equalsIgnoreCase("")){
            nominal_inq = "0";
        }if(admin_inq.equalsIgnoreCase("")){
            admin_inq = "0";
        }
        total = Integer.parseInt(nominal_inq)+Integer.parseInt(admin_inq);
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        loadingView = (ProgressBar)findViewById(R.id.loadingView);
        webView = (WebView)findViewById(R.id.webview);
        button_bayar = (Button)findViewById(R.id.button_inq_bayar);
        button_bayar.setOnClickListener(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadData(value,"text/html", "UTF-8");
        setToolbar();
        stringJson = new StringJson(this);
        print = new Print(this);
    }
    public void setToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.string_bpjs);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createPaymentListener(), createMyReqErrorListener());
            queue.add(post);
            loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private com.android.volley.Response.Listener<JSONObject> createPaymentListener(){
        final ArrayList<String> listTokenPln = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        String html = response.getString("response_desc");
                        struk_tercetak = response.getString("struk_tercetak");
                        webView.loadData(html,"text/html", "UTF-8");
                        setDialog(R.drawable.ic_check_success,R.layout.dialog_layout,
                                "PEMBAYARAN SUKSES", "Terimakasih! Pembayaran tagihan BPJS Kesehatan, Id Pel : " + nomor_va + ", Total Bayar " + FormatterText.CurencyIDR(total + "") + ", SUKSES Terbayar", "OK");
                        loadingView.setVisibility(View.GONE);
                        Log.d(TAG, "ulala : " + response.getString("response_code"));
                        button_bayar.setText("CETAK");
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc"),"OK");
                        loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                loadingView.setVisibility(View.GONE);
            }
        };
    }
    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else if(layoutType == R.layout.dialog_yes_no){
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView texDialogTitle = (TextView)dialog.findViewById(R.id.dialog_title);
            texDialogTitle.setText(title);

            TextView textDialog = (TextView)dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button buttonBatal = (Button) dialog.findViewById(R.id.dialog_button_batal);
            buttonBatal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            Button buttonYa = (Button) dialog.findViewById(R.id.dialog_button_ya);
            buttonYa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String json = stringJson.requestPaymentBPJSKES(code,nomor_va,month_value,id_inq,nominal_inq,admin_inq);
                    POSTrequest(json);
                    dialog.dismiss();
                }
            });


        }
        dialog.show();
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.button_inq_bayar) {
            if (button_bayar.getText().toString().equalsIgnoreCase("cetak")) {
                setDialog(R.drawable.ic_check_success,R.layout.dialog_layout, "CETAK STRUK", print.print(struk_tercetak), "OK");
            } else {
                setDialog(R.drawable.ic_help_yellow_600_18dp,R.layout.dialog_yes_no, "KONFIRMASI PEMBAYARAN",
                        "Apakah Anda yakin akan membayar tagihan BPJS Kesehatan,ID PEL : " + nomor_va + ", Total Bayar : " + FormatterText.CurencyIDR(total + ""), "");
            }
        }
    }
}
