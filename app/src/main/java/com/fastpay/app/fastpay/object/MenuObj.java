package com.fastpay.app.fastpay.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by skyshi on 03/02/17.
 */

public class MenuObj implements Parcelable{
    private String nameMenu;
    private String Code;

    public MenuObj (String nameMenu,String code){
        this.nameMenu = nameMenu;
        this.Code = code;
    }
    protected MenuObj(Parcel in) {
        nameMenu = in.readString();
        Code = in.readString();
    }

    public static final Creator<MenuObj> CREATOR = new Creator<MenuObj>() {
        @Override
        public MenuObj createFromParcel(Parcel in) {
            return new MenuObj(in);
        }

        @Override
        public MenuObj[] newArray(int size) {
            return new MenuObj[size];
        }
    };

    public String getNameMenu() {
        return nameMenu;
    }

    public void setNameMenu(String nameMenu) {
        this.nameMenu = nameMenu;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nameMenu);
        parcel.writeString(Code);
    }
}
