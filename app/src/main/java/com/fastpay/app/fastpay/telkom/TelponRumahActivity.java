package com.fastpay.app.fastpay.telkom;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.pln.FormCekIdPlnActivity;
import com.fastpay.app.fastpay.pln.ListrikPascaBayarActivity;
import com.fastpay.app.fastpay.request.JSONRequest;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TelponRumahActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "TelponRumahActivity";
    private static String MENU = "menu";
    private EditText edit_telepon,edit_kode_area;
    private Button button_cek;
    private String typeMenu;
    private Dialog dialog;
    private ProgressBar loadingView;
    private Toolbar toolbar;
    private StringJson stringJson;
    private InputMethodManager imm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telpon_rumah);
        stringJson = new StringJson(this);
        setToolbar(Config.TELKOM_TELEPON);
        imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        loadingView = (ProgressBar)findViewById(R.id.loadingView);
        edit_telepon = (EditText)findViewById(R.id.edit_telkom_telepon);
        edit_kode_area = (EditText)findViewById(R.id.edit_telkom_kodeArea);
        button_cek = (Button)findViewById(R.id.button_check);
        button_cek.setOnClickListener(this);
    }
    public void setToolbar(String title){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                finish();
        }
        return true;
    }

    public void POSTrequest(String json,String type){
        Log.d(TAG,type);
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            final JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createTeleponInquiry(), createMyReqErrorListener());
            queue.add(post);
            loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private com.android.volley.Response.Listener<JSONObject> createTeleponInquiry(){
        final ArrayList<String> listTokenPln = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        Log.d(TAG,"pasca");
                        String tagihan = response.getString("response_desc");
                        String id_inq = response.getString("reff_id_inq");
                        String nominal_inq = response.getString("nominal_inq");
                        String admin_inq = response.getString("nominal_admin_inq");
                        String kode_area = edit_kode_area.getText().toString();
                        String no_telepon = edit_telepon.getText().toString();
                        //Log.d(TAG,id);
                        Intent inquiry = new Intent(TelponRumahActivity.this, InquiryTelkom.class);
                        inquiry.putExtra("tagihan", tagihan);
                        inquiry.putExtra("kode_area", kode_area);
                        inquiry.putExtra("no_telepon", no_telepon);
                        inquiry.putExtra("id_inq", id_inq);
                        inquiry.putExtra("nominal_inq", nominal_inq);
                        inquiry.putExtra("admin_inq", admin_inq);
                        inquiry.putExtra("type", Config.TELKOM_TELEPON);
                        startActivity(inquiry);
                        loadingView.setVisibility(View.GONE);
                        edit_kode_area.setText("");
                        edit_telepon.setText("");
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                        loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }
    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,error.getMessage());
                loadingView.setVisibility(View.GONE);
            }
        };
    }

    public void setDialog(int icon,int layoutType,int title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else {
            dialog = new Dialog(this,R.style.AppTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_full_title);
            textDialogTitle.setText(title);

            TextView textDialogContentTitle = (TextView)dialog.findViewById(R.id.dialog_full_title_content);
            textDialogContentTitle.setText(contenttitle);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_full_content);
            textDialog.setText(content);

            ImageView backArrow = (ImageView)dialog.findViewById(R.id.back_arrow_dialog);
            backArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.button_check){
            if(edit_telepon.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"No Telepon harus diisi",0,"OK");
            }else if(edit_kode_area.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Kode Area harus diisi",0,"OK");
            } else{
                String json = stringJson.requestTeleponInquiry(edit_kode_area.getText().toString(),edit_telepon.getText().toString());
                POSTrequest(json, Config.TELKOM_TELEPON);
                //edit_id.setText("");
            }
        }
    }
}
