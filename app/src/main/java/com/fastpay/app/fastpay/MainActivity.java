package com.fastpay.app.fastpay;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.administrator.cekpulsa.CheckHargaPulsaFragment;
import com.fastpay.app.fastpay.administrator.checkkomisi.CheckKomisiFragment;
import com.fastpay.app.fastpay.administrator.komplain.KomplainFragment;
import com.fastpay.app.fastpay.administrator.pendaftaranmember.PendaftaranMemberFragment;
import com.fastpay.app.fastpay.administrator.transfer.TransferFragment;
import com.fastpay.app.fastpay.deposit.DepositFragment;
import com.fastpay.app.fastpay.fragment.FragmentHubungiKami;
import com.fastpay.app.fastpay.fragment.FragmentMenuUtama;
import com.fastpay.app.fastpay.fragment.FragmentSMSCenter;
import com.fastpay.app.fastpay.laporantransaksi.LaporanTransaksiActivity;
import com.fastpay.app.fastpay.login.LoginActivity;
import com.fastpay.app.fastpay.promo.Promo;
import com.fastpay.app.fastpay.request.JSONRequest;
import com.fastpay.app.fastpay.request.StringJson;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    /*migration*/
    private static final String TAG ="MainActivity";
    private PreferenceClass preferenceClass;
    private RelativeLayout relLeftDrawer,relMain;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private ImageView drawer_right_arrow;
    private TextView drawer_text_id_user,drawer_text_saldo;
    private Button drawer_button_deposit;
    private LinearLayout lin_child_drawer;
    private StringJson stringJson;
    private Dialog dialog;
    private Print print;
    private ScrollView scroll_drawer;
    private InputMethodManager imm;
    private LinearLayout lin_menu_keluar,lin_drawer_administrasi,lin_child_harga_pulsa,lin_drawer_menu_utama,
            lin_drawer_komisi,lin_drawer_komplain,lin_drawer_pendaftaran,lin_drawer_transfer,lin_drawer_menu_promo,
            lin_smscenter,lin_tes_print,lin_hubungi_kami,lin_kunci,lin_laporan_transaksi,lin_ubah_pin,lin_live_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        preferenceClass = new PreferenceClass(this);
        if(preferenceClass.isLoggedIn()){
            if(preferenceClass.isLocked()){
                Intent A = new Intent(MainActivity.this, KunciActivity.class);
                startActivity(A);
                finish();
            }
        }else{
            Intent A = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(A);
            finish();
        }
        stringJson = new StringJson(this);
        String json = stringJson.requestCekSaldo();
        POSTrequest(json,"saldo");
        drawerLayout = (DrawerLayout)findViewById(R.id.activity_main);
        relLeftDrawer = (RelativeLayout)findViewById(R.id.drawerLeft);
        relMain = (RelativeLayout)findViewById(R.id.mainView);
        drawer_right_arrow = (ImageView)findViewById(R.id.image_drawer_right_arrow);
        drawer_right_arrow.setOnClickListener(this);
        drawer_text_saldo = (TextView)findViewById(R.id.drawer_text_saldo);
        lin_child_drawer = (LinearLayout)findViewById(R.id.lin_child_drawer_menu);
        drawer_text_id_user = (TextView)findViewById(R.id.drawer_text_id_user);
        drawer_text_id_user.setText(preferenceClass.getLoggedUser().getId());
        drawer_button_deposit = (Button)findViewById(R.id.drawer_button_deposit);
        drawer_button_deposit.setOnClickListener(this);
        scroll_drawer = (ScrollView)findViewById(R.id.scroll_drawer);

        lin_drawer_menu_utama = (LinearLayout)findViewById(R.id.lin_drawer_menu_utama);
        lin_drawer_menu_utama.setOnClickListener(this);
        lin_menu_keluar = (LinearLayout)findViewById(R.id.lin_keluar);
        lin_menu_keluar.setOnClickListener(this);
        lin_drawer_administrasi = (LinearLayout)findViewById(R.id.lin_drawer_administrasi);
        lin_drawer_administrasi.setOnClickListener(this);
        lin_child_harga_pulsa = (LinearLayout)findViewById(R.id.lin_child_harga_pulsa);
        lin_child_harga_pulsa.setOnClickListener(this);
        lin_drawer_komisi = (LinearLayout)findViewById(R.id.lin_drawer_komisi);
        lin_drawer_komisi.setOnClickListener(this);
        lin_drawer_komplain = (LinearLayout)findViewById(R.id.lin_drawer_komplain);
        lin_drawer_komplain.setOnClickListener(this);
        lin_drawer_pendaftaran = (LinearLayout)findViewById(R.id.lin_drawer_pendaftaran);
        lin_drawer_pendaftaran.setOnClickListener(this);
        lin_drawer_transfer = (LinearLayout)findViewById(R.id.lin_child_transfer);
        lin_drawer_transfer.setOnClickListener(this);
        lin_drawer_menu_promo = (LinearLayout)findViewById(R.id.lin_drawer_menu_promo);
        lin_drawer_menu_promo.setOnClickListener(this);
        lin_smscenter = (LinearLayout)findViewById(R.id.lin_smscenter);
        lin_smscenter.setOnClickListener(this);
        lin_hubungi_kami = (LinearLayout)findViewById(R.id.lin_hubungi_kami);
        lin_hubungi_kami.setOnClickListener(this);
        lin_kunci = (LinearLayout)findViewById(R.id.lin_kunci);
        lin_kunci.setOnClickListener(this);
        lin_laporan_transaksi = (LinearLayout)findViewById(R.id.lin_laporan_transaksi);
        lin_laporan_transaksi.setOnClickListener(this);
        lin_ubah_pin = (LinearLayout)findViewById(R.id.lin_ubah_pin);
        lin_ubah_pin.setOnClickListener(this);
        lin_live_chat = (LinearLayout)findViewById(R.id.lin_live_chat);
        lin_live_chat.setOnClickListener(this);
        lin_tes_print = (LinearLayout)findViewById(R.id.lin_tes_print);
        lin_tes_print.setOnClickListener(this);

        toolbar = (Toolbar)findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_menu_burger);
        setDrawer();
        print = new Print(this);
        //set Fragment
        FragmentMenuUtama fragmentMenuUtama = new FragmentMenuUtama();
        setFragment(fragmentMenuUtama);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id ==R.id.action_right_drawer){
            Promo promo = new Promo();
            setFragment(promo);
        }
        if(drawerToggle.onOptionsItemSelected(item)){
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            if(drawerLayout.isDrawerVisible(relLeftDrawer)){
                drawerLayout.closeDrawer(relLeftDrawer);
            }else{
                drawerLayout.openDrawer(relLeftDrawer);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }
    public void setDrawer(){
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.app_null,R.string.app_null){
            @Override
            public void onDrawerClosed(View drawerView) {
                drawerLayout.closeDrawer(relLeftDrawer);
                lin_child_drawer.setVisibility(View.GONE);
                supportInvalidateOptionsMenu();
                drawerToggle.syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                drawerLayout.openDrawer(relLeftDrawer);
                supportInvalidateOptionsMenu();
                drawerToggle.syncState();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                drawerLayout.bringChildToFront(relLeftDrawer);
                drawerLayout.requestLayout();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(drawerToggle);
    }
    public void setFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        //fragmentTransaction.setCustomAnimations()
        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        drawerLayout.closeDrawer(relLeftDrawer);
        Log.d(TAG,getSupportFragmentManager().getBackStackEntryCount()+" ||");
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "ulala : " + "onhere"+TAG);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }*/
    public void POSTrequest(String json,String type){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = null;
            if(type.equalsIgnoreCase("saldo")) {
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, ceksaldo(), createMyReqErrorListener());
            }else if(type.equalsIgnoreCase("gantipin")){
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, gantipin(), createMyReqErrorListener());
            }
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private com.android.volley.Response.Listener<JSONObject> ceksaldo(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        String saldo = response.getString("response_value");
                        drawer_text_saldo.setText(drawer_text_saldo.getText()+" : "+FormatterText.CurencyIDR(saldo));
                    }else{
                        //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc"),"OK");
                        //loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }
    private com.android.volley.Response.Listener<JSONObject> gantipin(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        String value = response.getString("response_desc");
                        setDialog(R.drawable.ic_check_success,R.layout.dialog_layout,"GANTI PIN",value,0,"OK");
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc"),0,"OK");
                        //loadingView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
            }
        };
    }
    public void setDialog(int icon,int layoutType,String title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        if(id == R.id.image_drawer_right_arrow){
            if(lin_child_drawer.getVisibility() == View.GONE) {
                lin_child_drawer.setVisibility(View.VISIBLE);
            }else{
                lin_child_drawer.setVisibility(View.GONE);
            }
        }else if(id == R.id.lin_drawer_menu_utama){
            FragmentMenuUtama fragment = new FragmentMenuUtama();
            setFragment(fragment);
        }else if(id == R.id.lin_keluar){
            preferenceClass.clear();
            Intent out = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(out);
            finish();
        }else if(id == R.id.lin_drawer_administrasi){
            scroll_drawer.scrollTo(0,(int)lin_drawer_administrasi.getY());
            if(lin_child_drawer.getVisibility() == View.GONE) {
                lin_child_drawer.setVisibility(View.VISIBLE);
            }else{
                lin_child_drawer.setVisibility(View.GONE);
            }
        }else if(id == R.id.lin_child_harga_pulsa){
            CheckHargaPulsaFragment fragment = new CheckHargaPulsaFragment();
            setFragment(fragment);
        }else if(id == R.id.lin_drawer_komisi){
            CheckKomisiFragment fragment = new CheckKomisiFragment();
            setFragment(fragment);
        }else if(id == R.id.lin_drawer_komplain){
            KomplainFragment fragment = new KomplainFragment();
            setFragment(fragment);
        }else if(id == R.id.lin_drawer_pendaftaran){
            PendaftaranMemberFragment fragment = new PendaftaranMemberFragment();
            setFragment(fragment);
        }else if(id == R.id.lin_child_transfer){
            TransferFragment fragment = new TransferFragment();
            setFragment(fragment);
        }else if(id == R.id.drawer_button_deposit){
            DepositFragment depositFragment = new DepositFragment();
            setFragment(depositFragment);
        }else if(id == R.id.lin_drawer_menu_promo){
            Promo promo = new Promo();
            setFragment(promo);
        }else if(id == R.id.lin_smscenter){
            FragmentSMSCenter smscenter = new FragmentSMSCenter();
            setFragment(smscenter);
        }else if(id == R.id.lin_hubungi_kami){
            FragmentHubungiKami hubungi = new FragmentHubungiKami();
            setFragment(hubungi);
        }else if(id == R.id.lin_kunci){
            preferenceClass.setLocked();
            Intent out = new Intent(MainActivity.this,KunciActivity.class);
            startActivity(out);
            finish();
        }else if(id == R.id.lin_laporan_transaksi){
            LaporanTransaksiActivity laporan = new LaporanTransaksiActivity();
            setFragment(laporan);
        }else if(id == R.id.lin_ubah_pin){
            String json = stringJson.requestUbahPin();
            POSTrequest(json,"gantipin");
        }else if(id == R.id.lin_live_chat){
            Intent livechat = new Intent(MainActivity.this,WebViewActivity.class);
            livechat.putExtra("value","https://support.fastpay.co.id:4430/phplive/phplive.php?d=0&onpage=livechatimagelink&title=Live+Chat+Image+Link&namachat="
            +preferenceClass.getLoggedUser().getId());
            //livechat.putExtra("value","https://support.fastpay.co.id:4430/phplive/phplive.php?d=0&onpage=livechatimagelink&title=Live+Chat+Image+Link");
            startActivity(livechat);
        }else if(id == R.id.lin_tes_print){
            setDialog(R.drawable.ic_check_success,R.layout.dialog_layout, "TEST PRINT", print.print("TEST PRINT!!!!!"), 0,"OK");
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG,getSupportFragmentManager().getBackStackEntryCount()+"");
        if(getSupportFragmentManager().getBackStackEntryCount()>1){
            getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentMenuUtama fragment = new FragmentMenuUtama();
            setFragment(fragment);
        }else{
            finish();
        }
    }
}
