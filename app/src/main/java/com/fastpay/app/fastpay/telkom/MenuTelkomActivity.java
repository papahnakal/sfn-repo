package com.fastpay.app.fastpay.telkom;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.MenuCategoryAdapter;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.pln.FormCekIdPlnActivity;
import com.fastpay.app.fastpay.pln.MenuPlnActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MenuTelkomActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,MenuCategoryAdapter.ListenerList{
    ImageView search;
    ListView listMenu;
    ArrayList<MenuObj> menuTelkom = new ArrayList();
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_pln);
        search = (ImageView)findViewById(R.id.search_image);
        listMenu = (ListView)findViewById(R.id.list_menu_pln);
        setToolbar();
        List<String> StringMenu = Arrays.asList(getResources().getStringArray(R.array.telkom_menu));
        for (int i = 0; i <StringMenu.size(); i++) {
                MenuObj a = new MenuObj(StringMenu.get(i),"");
            menuTelkom.add(a);
        }
        //menuTelkom = new ArrayList<>(StringMenu);
        listMenu.setAdapter(new MenuCategoryAdapter(this,R.layout.list_category_main_menu,menuTelkom, Config.TELKOM,this));
        //listMenu.setOnItemClickListener(this);
    }
    public void setToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.string_telkom);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        /*String value = (String)adapterView.getItemAtPosition(i);
        if(value.equalsIgnoreCase("Telepon Rumah")){
            //Toast.makeText(this,"1",Toast.LENGTH_SHORT).show();
            Intent pascabayar = new Intent(MenuTelkomActivity.this,TelponRumahActivity.class);
            pascabayar.putExtra("menu","1");
            startActivity(pascabayar);
        }else if(value.equalsIgnoreCase("Speedy")) {
            //Toast.makeText(this,"2",Toast.LENGTH_SHORT).show();
            Intent prabayar = new Intent(MenuTelkomActivity.this, MenuSpeedyActivity.class);
            prabayar.putExtra("menu", "2");
            startActivity(prabayar);
        }*/
    }

    @Override
    public void onClickedList(MenuObj obj) {
        String value = obj.getNameMenu();
        if(value.equalsIgnoreCase("Telepon Rumah")){
            //Toast.makeText(this,"1",Toast.LENGTH_SHORT).show();
            Intent pascabayar = new Intent(MenuTelkomActivity.this,TelponRumahActivity.class);
            pascabayar.putExtra("menu","1");
            startActivity(pascabayar);
        }else if(value.equalsIgnoreCase("Speedy")) {
            //Toast.makeText(this,"2",Toast.LENGTH_SHORT).show();
            Intent prabayar = new Intent(MenuTelkomActivity.this, MenuSpeedyActivity.class);
            prabayar.putExtra("menu", "2");
            startActivity(prabayar);
        }
    }
}
