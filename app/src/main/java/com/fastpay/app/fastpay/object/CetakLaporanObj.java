package com.fastpay.app.fastpay.object;

/**
 * Created by papahnakal on 02/03/17.
 */

public class CetakLaporanObj {
    private String id_transaksi;
    private String keterangan;
    private String status_transaksi;
    public CetakLaporanObj(String id_transaksi,String keterangan,String status_transaksi){
        this.id_transaksi = id_transaksi;
        this.keterangan = keterangan;
        this.status_transaksi = status_transaksi;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStatus_transaksi() {
        return status_transaksi;
    }

    public void setStatus_transaksi(String status_transaksi) {
        this.status_transaksi = status_transaksi;
    }
}
