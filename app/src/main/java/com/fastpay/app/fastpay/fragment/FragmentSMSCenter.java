package com.fastpay.app.fastpay.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fastpay.app.fastpay.R;

/**
 * Created by skyshi on 23/02/17.
 */

public class FragmentSMSCenter extends Fragment implements View.OnClickListener{
    private static final String TAG = "fragment callcenter";
    LinearLayout lin_telkomsel,lin_ooredoo,lin_xl;
    TextView telkomsel,ooredo,xl;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View voew = inflater.inflate(R.layout.fragment_call_center,container,false);
        return voew;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lin_telkomsel = (LinearLayout)view.findViewById(R.id.lin_callcenter_list1);
        lin_telkomsel.setOnClickListener(this);
        lin_ooredoo = (LinearLayout)view.findViewById(R.id.lin_callcenter_list2);
        lin_ooredoo.setOnClickListener(this);
        lin_xl = (LinearLayout)view.findViewById(R.id.lin_callcenter_list3);
        lin_xl.setOnClickListener(this);
        telkomsel = (TextView)view.findViewById(R.id.txt_number1);
        ooredo = (TextView)view.findViewById(R.id.txt_number2);
        xl = (TextView)view.findViewById(R.id.txt_number3);
    }

    @Override
    public void onClick(View v) {
     int id =v.getId();
        if(id == R.id.lin_callcenter_list1){
            call(telkomsel.getText().toString());
        }else if(id == R.id.lin_callcenter_list2){
            call(ooredo.getText().toString());
        }else if(id == R.id.lin_callcenter_list3){
            call(xl.getText().toString());
        }
    }
    public void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+phone));
        startActivity(callIntent);
    }
}
