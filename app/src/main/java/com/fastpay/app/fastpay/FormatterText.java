package com.fastpay.app.fastpay;

/**
 * Created by skyshi on 26/01/17.
 */

public class FormatterText {
    public static String CurencyIDR(String curency){
        String result = "";
        int lenght = curency.length();
        if(lenght<=3){
            result = "Rp"+curency+",00";
        }else if((lenght>=4)&&(lenght<=6)){
            String reverse = new StringBuilder(curency).reverse().toString();
            String tigablkng = reverse.substring(0,3);
            String depan = reverse.substring(3,reverse.length());
            result = "Rp"+new StringBuilder(tigablkng+"."+depan).reverse().toString()+",00";
        }else if(lenght>=7&&lenght<=9){
            String reverse = new StringBuilder(curency).reverse().toString();
            String tigablkng = reverse.substring(0,3);
            String tigatengah = reverse.substring(3,6);
            String depan = reverse.substring(6,reverse.length());
            result ="Rp"+ new StringBuilder(tigablkng+"."+tigatengah+"."+depan).reverse().toString()+",00";
        }else if(lenght>=10&&lenght<=12){
            String reverse = new StringBuilder(curency).reverse().toString();
            String tigablkng = reverse.substring(0,3);
            String tigatengah = reverse.substring(3,6);
            String tigatengah2 = reverse.substring(6,9);
            String depan = reverse.substring(9,reverse.length());
            result ="Rp"+ new StringBuilder(tigablkng+"."+tigatengah+"."+tigatengah2+"."+depan).reverse().toString()+",00";
        }
        return result;
    }
    public static String leadZero(int number) {
        return (number < 10) ? "0" + String.valueOf(number) : String.valueOf(number);
    }
}
