package com.fastpay.app.fastpay.asuransi;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.request.JSONRequest;
import com.fastpay.app.fastpay.request.StringJson;
import com.fastpay.app.fastpay.tvkabel.TvKabelFormActivity;
import com.fastpay.app.fastpay.tvkabel.TvKabelInquiryActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class FormAsuransiActivity extends AppCompatActivity implements View.OnClickListener{
    private static String TAG = "tvKabel";
    private static String MENU = "menu";
    private EditText edit_id;
    private Button button_cek;
    private String code,title;
    private Dialog dialog;
    private JSONRequest jsonRequest;
    private StringJson stringJson;
    private ArrayAdapter<String> adapterSpinner ;
    private ProgressBar loadingView;
    private LinearLayout lin_pln_form;
    private Toolbar toolbar;
    private InputMethodManager imm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_asuransi);
        jsonRequest = new JSONRequest();
        stringJson = new StringJson(this);
        Intent intent = getIntent();
        code = intent.getStringExtra("code");
        title = intent.getStringExtra("title");
        Log.d(TAG,code);
        imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        loadingView = (ProgressBar)findViewById(R.id.loadingView);
        edit_id = (EditText)findViewById(R.id.edit_id_pelanggan);
        button_cek = (Button)findViewById(R.id.button_check);
        lin_pln_form = (LinearLayout)findViewById(R.id.lin_form);
        button_cek.setOnClickListener(this);
        setToolbar(title);
    }
    public void setToolbar(String title){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                finish();
        }
        return true;
    }
    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loadingView.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createInquiry(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private com.android.volley.Response.Listener<JSONObject> createInquiry(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        loadingView.setVisibility(View.GONE);
                        String value = response.getString("response_desc");
                        String reff_id_inq = response.getString("reff_id_inq");
                        String nominal_admin_inq = response.getString("nominal_admin_inq");
                        String nominal_inq = response.getString("nominal_inq");
                        //webview.loadData(value,"text/html", "UTF-8");
                        loadingView.setVisibility(View.GONE);
                        Intent a = new Intent(FormAsuransiActivity.this,InquiryAsuransiActivity.class);
                        a.putExtra("id_inq",reff_id_inq);
                        a.putExtra("admin_inq",nominal_admin_inq);
                        a.putExtra("nominal_inq",nominal_inq);
                        a.putExtra("value",value);
                        a.putExtra("code",code);
                        a.putExtra("id_pelanggan",edit_id.getText().toString());
                        startActivity(a);
                        edit_id.setText("");
                        //webview_komisi.loadData(value,"text/html", "UTF-8");
                    }else{
                        loadingView.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,response.getString("response_desc"),0,"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loadingView.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }
    public void setDialog(int icon,int layoutType,int title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
    @Override
    public void onClick(View view) {
        if(edit_id.getText().toString().equalsIgnoreCase("")){
            setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Masukkan No Identitas",0,"OK");
        }else{
            String json = stringJson.requestInquiryAsuransi(edit_id.getText().toString(),code);
            POSTrequest(json);
        }
    }
}
