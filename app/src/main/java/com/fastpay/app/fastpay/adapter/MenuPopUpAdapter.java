package com.fastpay.app.fastpay.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.object.MenuObj;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by skyshi on 03/02/17.
 */

public class MenuPopUpAdapter extends BaseAdapter{
    private ArrayList<MenuObj> titles = new ArrayList<>();
    private ArrayList<MenuObj> tempTitles = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private ListenerList listener;
    private Context context;
    private int positionselected = 0;
    private String strFilterText="";
    int resource;
    public MenuPopUpAdapter(Context context, int resource, ArrayList<MenuObj> objects,ListenerList listener) {
        for (int i = 0; i <objects.size() ; i++) {
            if(this.titles.size()>0){
                if(!this.titles.contains(objects.get(i))){
                    this.titles.add(objects.get(i));
                    this.tempTitles.add(objects.get(i));
                }
            }else{
                this.titles.add(objects.get(i));
                this.tempTitles.add(objects.get(i));
            }
        }
        this.resource = resource;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder viewHolder;
        if(convertView==null) {
            convertView = infl.inflate(resource, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtName = (TextView)convertView.findViewById(R.id.title_menu);
            viewHolder.arrow_right = (ImageView)convertView.findViewById(R.id.arrow_right);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        if(!titles.get(position).getNameMenu().isEmpty()){
            viewHolder.txtName.setText(titles.get(position).getNameMenu());
        }
        viewHolder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickedList(titles.get(position));
            }
        });
        viewHolder.arrow_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickedList(titles.get(position));
            }
        });
        return convertView;
    }
    public void filter(String charText) {
        charText   = charText.toLowerCase(Locale.getDefault());
        strFilterText = charText;
        titles.clear();
        if (charText.length() == 0||charText.equalsIgnoreCase("")) {
            titles.addAll(tempTitles);
        } else {
            /*for (Brands wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    arrayBrandslist.add(wp);
                }
            }*/
            for (int i = 0; i <tempTitles.size() ; i++) {
                if(tempTitles.get(i).getNameMenu().toLowerCase(Locale.getDefault()).contains(charText)){
                    titles.add(tempTitles.get(i));
                }
            }
            /*if (phoneList.size()==0) {
                Brands brands = new Brands(context.getResources().getString(R.string.no_brands_found), "0", "nobrandsfound");
                arrayBrandslist.add(brands);
            }*/
        }
        notifyDataSetChanged();
    }
    public interface ListenerList{
        public void onClickedList(MenuObj obj);
    }
    private static class ViewHolder{
        private TextView txtName;
        private ImageView arrow_right;
    }
}
