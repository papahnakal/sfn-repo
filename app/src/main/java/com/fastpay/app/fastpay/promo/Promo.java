package com.fastpay.app.fastpay.promo;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.PromoAdapter;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Promo extends Fragment implements AdapterView.OnItemClickListener{
    private static final String TAG = "promo";
    private MenuObj menuList;
    private ArrayList<MenuObj> menuArray ;
    private ProgressBar loading;
    private StringJson stringJson;
    private Dialog dialog;
    private ListView listPromo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_promo,container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stringJson = new StringJson(getActivity());
        listPromo = (ListView)view.findViewById(R.id.list_promo);
        String json = stringJson.requestCarousel();
        POSTrequest(json);
        listPromo.setOnItemClickListener(this);
    }
    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createListPromo(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private com.android.volley.Response.Listener<JSONObject> createListPromo(){
        menuArray = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        JSONArray array = response.getJSONArray("response_value");
                        for (int i = 0; i <array.length() ; i++) {
                            JSONObject promo = (JSONObject) array.get(i);
                            String promo_image = promo.getString("url_image");
                            String promo_target = promo.getString("target_link");
                            Log.d(TAG, "ulala : " + promo_image+"|"+promo_target);
                            menuList = new MenuObj(promo_image,promo_target);
                            menuArray.add(menuList);
                        }
                        listPromo.setAdapter(new PromoAdapter(getActivity(),R.layout.list_promo,menuArray));
                    }else{
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }
    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String link = menuArray.get(i).getCode().toString();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(link));
        startActivity(intent);
    }
}
