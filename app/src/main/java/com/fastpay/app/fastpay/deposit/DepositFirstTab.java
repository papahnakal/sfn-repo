package com.fastpay.app.fastpay.deposit;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.MenuPopUp;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skyshi on 02/02/17.
 */

public class DepositFirstTab extends Fragment implements View.OnClickListener{
    private static final String TAG = "DepositFirstTab";
    private EditText edit_deposit_pilihbank,edit_deposit_amount;
    private MenuObj menuList;
    private ArrayList<MenuObj> menuArray ;
    private StringJson stringJson;
    private Dialog dialog;
    private Button button_ambil_tiket;
    private String code_bank;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_first_tab,container,false);
        return v;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edit_deposit_pilihbank = (EditText)view.findViewById(R.id.edit_deposit_pilihbank);
        edit_deposit_pilihbank.setOnClickListener(this);
        edit_deposit_amount = (EditText)view.findViewById(R.id.edit_deposit_nominal_deposit);
        button_ambil_tiket = (Button)view.findViewById(R.id.button_deposit_ambil_tiket);
        button_ambil_tiket.setOnClickListener(this);
        stringJson = new StringJson(getActivity());
        String json = stringJson.requestBank();
        Log.d(TAG, "ulala : " + json);
        POSTrequest(json,"bank");
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.edit_deposit_pilihbank){
            Intent A = new Intent(getActivity(),MenuPopUp.class);
            A.putExtra("type",Config.RES_DEPOSIFIRST);
            A.putParcelableArrayListExtra("list",menuArray);
            startActivityForResult(A, Config.RES_DEPOSIFIRST);
        }else if(id==R.id.button_deposit_ambil_tiket){
            if(edit_deposit_amount.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),"Nominal Deposit harus diisi","OK");
            }else if(edit_deposit_pilihbank.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),"Bank harus dipilih","OK");
            }else {
                String json = stringJson.requestDeposit(code_bank, edit_deposit_amount.getText().toString());
                POSTrequest(json, "deposit");
            }
        }
    }

    public void POSTrequest(String json,String type){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            //loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = null;
            if(type.equalsIgnoreCase("bank")) {
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createListMenu(), createMyReqErrorListener());
            }else if(type.equalsIgnoreCase("deposit")){
                post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createDeposit(), createMyReqErrorListener());
            }
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //getActivity();
        //Log.d(TAG, "ulala : " + "onhere"+TAG+data.getStringExtra("name"));
        if(data!=null) {
            if (resultCode == Activity.RESULT_OK) {
                edit_deposit_pilihbank.setText(data.getStringExtra("name"));
                code_bank = data.getStringExtra("code");
            } else {
                edit_deposit_pilihbank.setText("");
            }
        }
    }
    private com.android.volley.Response.Listener<JSONObject> createDeposit(){
        menuArray = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        setDialog(R.drawable.ic_check_success,R.layout.dialog_layout,"TIKET DEPOSIT",response.getString("response_desc").toString(),"OK");
                        edit_deposit_pilihbank.setText("");
                        edit_deposit_amount.setText("");
                        //loading.setVisibility(View.GONE);
                    }else{
                        //loading.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }
    private com.android.volley.Response.Listener<JSONObject> createListMenu(){
        menuArray = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        JSONArray array = response.getJSONArray("response_value");
                        for (int i = 0; i <array.length() ; i++) {
                            JSONObject bank = (JSONObject) array.get(i);
                            String bank_code = bank.getString("bank_code");
                            String bank_name = bank.getString("bank_name");
                            Log.d(TAG, "ulala : " + bank_code + "" + bank_code);
                            menuList = new MenuObj(bank_name,bank_code);
                            menuArray.add(menuList);
                        }
                        Log.d(TAG, "ulala : " + response.getString("response_code"));
                        //loading.setVisibility(View.GONE);
                    }else{
                        //loading.setVisibility(View.GONE);
                        //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }
    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                //loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }
    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
}
