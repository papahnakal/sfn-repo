package com.fastpay.app.fastpay.administrator.checkkomisi;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by skyshi on 27/01/17.
 */

public class CheckKomisiFragment extends Fragment implements View.OnClickListener{
    private static final String TAG = "checkKomisiFragment";
    private EditText edit_komisi_password;
    private Button button_komisi_kirim;
    private WebView webview_komisi;
    private TextView text_result_komisi;
    private CardView cardView_komisi;
    private ImageView imageClose;
    private StringJson stringJson;
    private ProgressBar loading;
    private Dialog dialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_check_komisi,container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stringJson = new StringJson(getActivity());
        edit_komisi_password = (EditText)view.findViewById(R.id.edit_komisi_password);
        button_komisi_kirim = (Button)view.findViewById(R.id.button_komisi_kirim);
        button_komisi_kirim.setOnClickListener(this);
        loading = (ProgressBar)view.findViewById(R.id.loadingView);
        webview_komisi = (WebView)view.findViewById(R.id.webview_komisi);
        text_result_komisi = (TextView)view.findViewById(R.id.text_result_komisi);
        cardView_komisi = (CardView)view.findViewById(R.id.card_view_komisi);
        imageClose = (ImageView)view.findViewById(R.id.image_close_komisi_result);
        imageClose.setOnClickListener(this);
    }

    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createKomisiListener(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private com.android.volley.Response.Listener<JSONObject> createKomisiListener(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {

                        String value = response.getString("response_desc");
                        text_result_komisi.setText(value);
                        cardView_komisi.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        //webview_komisi.loadData(value,"text/html", "UTF-8");
                    }else{
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }
    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
    @Override
    public void onClick(View view) {
        int id  =  view.getId();
        if(id == R.id.button_komisi_kirim){
            if(!edit_komisi_password.getText().toString().equalsIgnoreCase("")) {
                String json = stringJson.requestKomisi(edit_komisi_password.getText().toString());
                POSTrequest(json);
            }else {
                setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),getString(R.string.string_warning_pin_atau_password),"OK");
            }
        }else if(id == R.id.image_close_komisi_result){
            cardView_komisi.setVisibility(View.GONE);
            edit_komisi_password.setText("");
        }
    }
}
