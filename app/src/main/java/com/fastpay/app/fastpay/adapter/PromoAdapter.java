package com.fastpay.app.fastpay.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.object.MenuObj;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by skyshi on 17/02/17.
 */

public class PromoAdapter extends ArrayAdapter<MenuObj> {
    private ArrayList<MenuObj> titles = new ArrayList<>();
    private Context context;
    int resource;

    public PromoAdapter(Context context, int resource, ArrayList<MenuObj> objects) {
        super(context, 0, objects);
        this.titles = objects;
        this.resource = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);
        }
        ImageView image = (ImageView)convertView.findViewById(R.id.image_promo);
        Picasso.with(context).load(titles.get(position).getNameMenu()).placeholder(R.drawable.grha_bimasakti).fit().centerCrop().into(image);
        return convertView;
    }
}
