package com.fastpay.app.fastpay;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fastpay.app.fastpay.login.LoginActivity;

public class KunciActivity extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout lin_masuk,lin_keluar,lin_hapus,lin_0,lin_1,lin_2,lin_3,lin_4,lin_5,lin_6,lin_7,lin_8,lin_9;
    private EditText edit_key;
    private PreferenceClass preferenceClass;
    private Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kunci);
        preferenceClass = new PreferenceClass(this);
        lin_0 = (LinearLayout)findViewById(R.id.lin_key_0);
        lin_0.setOnClickListener(this);
        lin_1 = (LinearLayout)findViewById(R.id.lin_key_1);
        lin_1.setOnClickListener(this);
        lin_2 = (LinearLayout)findViewById(R.id.lin_key_2);
        lin_2.setOnClickListener(this);
        lin_3 = (LinearLayout)findViewById(R.id.lin_key_3);
        lin_3.setOnClickListener(this);
        lin_4 = (LinearLayout)findViewById(R.id.lin_key_4);
        lin_4.setOnClickListener(this);
        lin_5 = (LinearLayout)findViewById(R.id.lin_key_5);
        lin_5.setOnClickListener(this);
        lin_6 = (LinearLayout)findViewById(R.id.lin_key_6);
        lin_6.setOnClickListener(this);
        lin_7 = (LinearLayout)findViewById(R.id.lin_key_7);
        lin_7.setOnClickListener(this);
        lin_8 = (LinearLayout)findViewById(R.id.lin_key_8);
        lin_8.setOnClickListener(this);
        lin_9 = (LinearLayout)findViewById(R.id.lin_key_9);
        lin_9.setOnClickListener(this);
        lin_hapus = (LinearLayout)findViewById(R.id.lin_key_hapus);
        lin_hapus.setOnClickListener(this);
        lin_keluar = (LinearLayout)findViewById(R.id.lin_key_keluar);
        lin_keluar.setOnClickListener(this);
        lin_masuk = (LinearLayout)findViewById(R.id.lin_key_masuk);
        lin_masuk.setOnClickListener(this);
        edit_key = (EditText)findViewById(R.id.edit_key_unlock);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id==R.id.lin_key_0){
            edit_key.setText(edit_key.getText()+"0");
        }else if (id==R.id.lin_key_1){
            edit_key.setText(edit_key.getText()+"1");
        }else if (id==R.id.lin_key_2){
            edit_key.setText(edit_key.getText()+"2");
        }else if (id==R.id.lin_key_3){
            edit_key.setText(edit_key.getText()+"3");
        }else if (id==R.id.lin_key_4){
            edit_key.setText(edit_key.getText()+"4");
        }else if (id==R.id.lin_key_5){
            edit_key.setText(edit_key.getText()+"5");
        }else if (id==R.id.lin_key_6){
            edit_key.setText(edit_key.getText()+"6");
        }else if (id==R.id.lin_key_7){
            edit_key.setText(edit_key.getText()+"7");
        }else if (id==R.id.lin_key_8){
            edit_key.setText(edit_key.getText()+"8");
        }else if (id==R.id.lin_key_9){
            edit_key.setText(edit_key.getText()+"9");
        }else if (id==R.id.lin_key_masuk){
            if (preferenceClass.getPin().equalsIgnoreCase(edit_key.getText().toString())){
                preferenceClass.setUnlock();
                Intent out = new Intent(KunciActivity.this,MainActivity.class);
                startActivity(out);
                finish();
            }else if(edit_key.getText().toString().equalsIgnoreCase("")){
                setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Anda belum memasukkan pin",0,"OK");
            }else{
                setDialog(R.drawable.close_red,R.layout.dialog_layout,R.string.string_peringatan,"Pin yang Anda masukan salah",0,"OK");
            }
        }else if (id==R.id.lin_key_keluar){
            finish();
        }else if (id==R.id.lin_key_hapus){
            edit_key.setText("");
        }
    }
    public void setDialog(int icon,int layoutType,int title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else {
            dialog = new Dialog(this,R.style.AppTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_full_title);
            textDialogTitle.setText(title);

            TextView textDialogContentTitle = (TextView)dialog.findViewById(R.id.dialog_full_title_content);
            textDialogContentTitle.setText(contenttitle);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_full_content);
            textDialog.setText(content);

            ImageView backArrow = (ImageView)dialog.findViewById(R.id.back_arrow_dialog);
            backArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
}
