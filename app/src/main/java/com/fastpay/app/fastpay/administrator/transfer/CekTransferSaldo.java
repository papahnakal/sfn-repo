package com.fastpay.app.fastpay.administrator.transfer;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.FormatterText;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONException;
import org.json.JSONObject;

public class CekTransferSaldo extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG ="CekTransferSaldo";
    private String id_tujuan,nominal,pin,berita;
    private ProgressBar loading;
    private Button button_transfer;
    private StringJson stringJson;
    private Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_transfer_saldo);
        loading = (ProgressBar)findViewById(R.id.loadingView);
        stringJson = new StringJson(this);
        Intent a = getIntent();
        id_tujuan = a.getStringExtra("id");
        nominal = a.getStringExtra("nominal");
        pin = a.getStringExtra("pin");
        berita = a.getStringExtra("berita");
        button_transfer = (Button)findViewById(R.id.button_transfer);
        button_transfer.setOnClickListener(this);
    }

    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createTransferListener(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private com.android.volley.Response.Listener<JSONObject> createTransferListener(){
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {

                        String value = response.getString("response_desc");
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.ic_check_success,R.layout.dialog_layout,getString(R.string.string_transfer),response.getString("response_desc"),"OK");
                        button_transfer.setText("CETAK");
                        //webview_komisi.loadData(value,"text/html", "UTF-8");
                    }else{
                        loading.setVisibility(View.GONE);
                        setDialog(R.drawable.close_red,R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc"),"OK");
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }

    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else if(layoutType == R.layout.dialog_yes_no){
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView texDialogTitle = (TextView)dialog.findViewById(R.id.dialog_title);
            texDialogTitle.setText(title);

            TextView textDialog = (TextView)dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button buttonBatal = (Button) dialog.findViewById(R.id.dialog_button_batal);
            buttonBatal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            Button buttonYa = (Button) dialog.findViewById(R.id.dialog_button_ya);
            buttonYa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String json = stringJson.requestTransfer(id_tujuan,nominal,pin);
                    POSTrequest(json);
                    dialog.dismiss();
                }
            });

        }
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.button_transfer){
            setDialog(R.drawable.ic_help_yellow_600_18dp,R.layout.dialog_yes_no,"KONFIRMASI TRANSFER",
                    getString(R.string.string_konfirmasi_transfer)+" ID : "+id_tujuan+",dengan nominal : "+ FormatterText.CurencyIDR(nominal+""),"");
        }
    }
}
