package com.fastpay.app.fastpay.laporantransaksi;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fastpay.app.fastpay.Print;
import com.fastpay.app.fastpay.R;

public class CUlaporan extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG="culaporan";
    private WebView webview;
    private Dialog dialog;
    private Toolbar toolbar;
    private ProgressBar loadingView;
    private Button button_bayar;
    private String value,struk_tercetak;
    private Print print;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_culaporan);
        Intent intent = getIntent();
        value = intent.getStringExtra("value");
        struk_tercetak = intent.getStringExtra("struk_tercetak");
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        loadingView = (ProgressBar)findViewById(R.id.loadingView);
        webview = (WebView)findViewById(R.id.webview);
        button_bayar = (Button)findViewById(R.id.button_cetak);
        button_bayar.setOnClickListener(this);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadData(value,"text/html","UTF-8");
        setToolbar();
        print = new Print(this);
    }
    public void setToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("CETAK TIKET");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
    public void setDialog(int icon,int layoutType,String title,String content,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(icon);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else if(layoutType == R.layout.dialog_yes_no){
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView texDialogTitle = (TextView)dialog.findViewById(R.id.dialog_title);
            texDialogTitle.setText(title);

            TextView textDialog = (TextView)dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button buttonBatal = (Button) dialog.findViewById(R.id.dialog_button_batal);
            buttonBatal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            Button buttonYa = (Button) dialog.findViewById(R.id.dialog_button_ya);
            buttonYa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*String json = stringJson.requestPaymentTelpPasca(code,id_pelanggan,id_inq,nominal_inq,admin_inq);
                    POSTrequest(json);
                    dialog.dismiss();*/
                }
            });


        }
        dialog.show();
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.button_cetak){
            setDialog(R.drawable.ic_check_success,R.layout.dialog_layout, "CETAK STRUK", print.print(struk_tercetak), "OK");
        }
    }
}
