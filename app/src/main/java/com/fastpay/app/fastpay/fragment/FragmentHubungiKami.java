package com.fastpay.app.fastpay.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fastpay.app.fastpay.R;

/**
 * Created by skyshi on 23/02/17.
 */

public class FragmentHubungiKami extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View voew = inflater.inflate(R.layout.fragment_hubungi,container,false);
        return voew;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
