package com.fastpay.app.fastpay.asuransi;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.MenuCategoryAdapter;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.bpjs.BPJSKSActivity;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.pdam.MenuPdamActivity;
import com.fastpay.app.fastpay.pln.MenuPlnActivity;
import com.fastpay.app.fastpay.request.StringJson;
import com.fastpay.app.fastpay.tvkabel.TvKabelActivity;
import com.fastpay.app.fastpay.tvkabel.TvKabelFormActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MenuAsuransiActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener,MenuCategoryAdapter.ListenerList{
    private static final String TAG ="MenuAsuransiActivity";
    private ImageView search,icon_cross;
    private ListView listMenu;
    //private ArrayList<String> productList;
    //private ArrayList<String> productCode;
    private ArrayList<MenuObj> menu;
    private StringJson stringJson;
    private Toolbar toolbar;
    private ProgressBar loading;
    private EditText searchText;
    private MenuCategoryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_asuransi);
        search = (ImageView)findViewById(R.id.search_image);
        icon_cross = (ImageView)findViewById(R.id.icon_cross);
        icon_cross.setOnClickListener(this);
        listMenu = (ListView)findViewById(R.id.list_menu_pln);
        loading = (ProgressBar)findViewById(R.id.loadingView);
        searchText = (EditText)findViewById(R.id.edit_cari);

        setToolbar();
        ///List<String> StringMenu = Arrays.asList(getResources().getStringArray(R.array.pln_menu));
        //productList = new ArrayList<>(StringMenu);
        stringJson = new StringJson(this);
        String json = stringJson.requestListProduct(Config.ASURANSI);
        Log.d(TAG,json);
        POSTrequest(json);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String src = s.toString().toLowerCase(Locale.getDefault());
                if (src.toString().length() != 0) {
                    adapter.filter(src);
                } else {
                    adapter.filter("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //listMenu.setOnItemClickListener(this);
    }
    public void setToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.string_asuransi);
    }
    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createListProduct(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private com.android.volley.Response.Listener<JSONObject> createListProduct(){
        //productList = new ArrayList<>();
        //productCode = new ArrayList<>();
        menu= new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        JSONArray array = response.getJSONArray("response_value");
                        Log.d(TAG,"oke");
                        for (int i = 0; i <array.length() ; i++) {
                            JSONObject product = (JSONObject) array.get(i);
                            String name = product.getString("product_name");
                            String code = product.getString("product_code");
                            Log.d(TAG,name+" "+code);
                            MenuObj a = new MenuObj(name,code);
                            menu.add(a);
                            //productList.add(name);
                            //productCode.add(code);
                        }
                        Log.d(TAG,menu.size()+"");
                        adapter = new MenuCategoryAdapter(
                                getApplicationContext(),R.layout.list_category_main_menu,menu,Config.ASURANSI,MenuAsuransiActivity.this
                        );
                        listMenu.setAdapter(adapter);
                        loading.setVisibility(View.GONE);
                        //webview_komisi.loadData(value,"text/html", "UTF-8");
                    }else{
                        loading.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent a = new Intent(MenuAsuransiActivity.this,FormAsuransiActivity.class);
        //a.putExtra("code",productCode.get(i).toString());
        startActivity(a);
    }

    @Override
    public void onClickedList(MenuObj obj) {
        if(obj.getCode().toString().equalsIgnoreCase("ASRBPJSKS")){
            Intent a = new Intent(MenuAsuransiActivity.this, BPJSKSActivity.class);
            a.putExtra("code", obj.getCode());
            a.putExtra("title", obj.getNameMenu());
            startActivity(a);
        }else {
            Intent a = new Intent(MenuAsuransiActivity.this, FormAsuransiActivity.class);
            a.putExtra("code", obj.getCode());
            a.putExtra("title", obj.getNameMenu());
            startActivity(a);
        }
    }

    @Override
    public void onClick(View v) {
     int id = v.getId();
        if (id == R.id.icon_cross){
            searchText.setText("");
        }
    }
}
