package com.fastpay.app.fastpay.game;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.MenuCategoryAdapter;
import com.fastpay.app.fastpay.adapter.utils.MyVolley;
import com.fastpay.app.fastpay.object.MenuObj;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class MenuGameActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener,MenuCategoryAdapter.ListenerList{
    private static final String TAG ="MenuGameActivity";
    private ImageView search,icon_cross;
    private ListView listMenu;
    private EditText searchText;
    //private ArrayList<String> productList;
    //private ArrayList<String> productCode;
    private ArrayList<MenuObj> menu;
    private StringJson stringJson;
    private Toolbar toolbar;
    private ProgressBar loading;
    private MenuCategoryAdapter menuCategoryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_game);
        search = (ImageView)findViewById(R.id.search_image);
        icon_cross = (ImageView)findViewById(R.id.icon_cross);
        icon_cross.setOnClickListener(this);
        listMenu = (ListView)findViewById(R.id.list_menu_pln);
        loading = (ProgressBar)findViewById(R.id.loadingView);
        setToolbar();
        ///List<String> StringMenu = Arrays.asList(getResources().getStringArray(R.array.pln_menu));
        //productList = new ArrayList<>(StringMenu);
        stringJson = new StringJson(this);
        String json = stringJson.requestProviderGame();
        Log.d(TAG,json);
        POSTrequest(json);
        searchText = (EditText)findViewById(R.id.edit_cari);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String src = charSequence.toString().toLowerCase(Locale.getDefault());
                if (src.toString().length() != 0) {
                    menuCategoryAdapter.filter(src);
                } else {
                    menuCategoryAdapter.filter("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        //listMenu.setOnItemClickListener(this);
    }
    public void setToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.string_game);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
    public void POSTrequest(String json){
        RequestQueue queue = MyVolley.getRequestQueue();
        try {
            loading.setVisibility(View.VISIBLE);
            JSONObject jsonBody = new JSONObject(json);
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Config.BASE_URL, jsonBody, createListProduct(), createMyReqErrorListener());
            queue.add(post);
            //loadingView.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private com.android.volley.Response.Listener<JSONObject> createListProduct(){
        //productList = new ArrayList<>();
        //productCode = new ArrayList<>();
        menu = new ArrayList<>();
        return new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("response_code").equalsIgnoreCase("00")) {
                        JSONArray array = response.getJSONArray("response_value");
                        Log.d(TAG,"oke");
                        for (int i = 0; i <array.length() ; i++) {
                            JSONObject product = (JSONObject) array.get(i);
                            String name = product.getString("provider_name");
                            String code = product.getString("provider_code");
                            Log.d(TAG,name+" "+code);
                            MenuObj a = new MenuObj(name,code);
                            menu.add(a);
                            //productList.add(name);
                            //productCode.add(code);
                        }
                        Log.d(TAG,menu.size()+"");
                        menuCategoryAdapter = new MenuCategoryAdapter(getApplicationContext(),R.layout.list_category_main_menu,menu,Config.GAME,MenuGameActivity.this);
                        listMenu.setAdapter(menuCategoryAdapter);
                        loading.setVisibility(View.GONE);
                        //webview_komisi.loadData(value,"text/html", "UTF-8");
                    }else{
                        loading.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG,e.getMessage());
                }
            }
        };
    }

    private com.android.volley.Response.ErrorListener createMyReqErrorListener() {
        return new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d(TAG,error.getMessage());
                //loadingView.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                //setDialog(R.layout.dialog_layout,getString(R.string.string_peringatan),response.getString("response_desc").toString(),"OK");
            }
        };
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent a = new Intent(MenuGameActivity.this,Menu2GameActivity.class);
        //a.putExtra("code",productCode.get(i).toString());
        startActivity(a);
    }

    @Override
    public void onClickedList(MenuObj obj) {
        Intent a = new Intent(MenuGameActivity.this,Menu2GameActivity.class);
        a.putExtra("code",obj.getCode().toString());
        Log.d(TAG,obj.getCode()+"~~~");
        startActivity(a);
    }

    @Override
    public void onClick(View v) {
     int id = v.getId();
        if(id == R.id.icon_cross){
            searchText.setText("");
        }
    }
}
