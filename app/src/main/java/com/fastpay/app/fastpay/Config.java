package com.fastpay.app.fastpay;

/**
 * Created by skyshi on 19/01/17.
 */

public class Config {
    public static final String BASE_URL = "http://dev.apimobilesbf.fastpay.co.id/";
    /*PLN*/
    public static final String PASCABAYAR = "pascabayar";
    public static final String PRABAYAR = "prabayar";
    public static final String NONTALGIS = "nontalgis";
    public static final String PLNNON = "PLNNON";
    public static final String PLNPRA = "PLNPRA";
    public static final String PLNPASC = "PLNPASC";
    public static final String PLN = "PLN";
    public static final String TV_KABEL = "TV_KABEL";

    /*ADMIN*/
    public static final String MENU = "MENU";
    public static final String CHECKPULSA = "CHECKPULSA";
    /*PENDAFTARAN*/
    public static final String PROPINSI = "PROPINSI";
    public static final String KOTA = "KOTA";
    public static final String LOCKET = "LOCKET";
    public static final String DAFTAR = "DAFTAR";
    /*Activity result*/
    public static final int RES_DEPOSIFIRST = 1;
    public static final int RES_KOMPLAIN = 2;
    public static final int RES_PROPINSI = 3;
    public static final int RES_KOTA = 4;
    public static final int RES_LOKET = 5;
    public static final int RES_LIST_PULSA = 6;
    public static final int RES_BPJS = 7;
    public static final int RES_PLN = 8;
    /*ASURANSI*/
    public static final String ASURANSI = "ASURANSI";
    public static final String BPJS = "BPJS";
    /*PDAM*/
    public static final String PDAM = "PDAM";
    /*GAME*/
    public static final String GAME = "GAME ONLINE";
    public static final String PULSA = "PULSA";
    public static final String CICILAN = "CICILAN";
    public static final String HPPASCABAYAR = "HPPASCABAYAR";
    public static final String KREDIT = "KREDIT";
    /*TELKOM*/
    public static final String TELKOM = "TELKOM";
    public static final String TELKOM_TELEPON = "TELEPON";
    public static final String TELKOM_SPEEDY = "SPEEDY";
}
