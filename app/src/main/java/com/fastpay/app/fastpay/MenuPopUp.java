package com.fastpay.app.fastpay;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.fastpay.app.fastpay.adapter.MenuCategoryAdapter;
import com.fastpay.app.fastpay.adapter.MenuPopUpAdapter;
import com.fastpay.app.fastpay.object.MenuObj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by skyshi on 03/02/17.
 */

public class MenuPopUp extends AppCompatActivity implements AdapterView.OnItemClickListener,MenuPopUpAdapter.ListenerList,View.OnClickListener{
    private ListView listMenu;
    private ArrayList<MenuObj> menuPLN ;
    private Toolbar toolbar;
    private EditText edit_search_popupmenu;
    private int type;
    private MenuPopUpAdapter adapter;
    private String name = "";
    private String code = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_pop_up);
        ImageView imagecros = (ImageView)findViewById(R.id.icon_cross);
        imagecros.setOnClickListener(this);
        ImageView searchicon = (ImageView)findViewById(R.id.search_image);
        Intent intent = getIntent();
        menuPLN = intent.getParcelableArrayListExtra("list");
        type = intent.getIntExtra("type",0);
        Log.d("list",menuPLN.size()+"");
        listMenu = (ListView)findViewById(R.id.list_popup_menu);
        if(type == Config.RES_LIST_PULSA) {
            imagecros.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            setToolbar(Config.PULSA);
        }else if(type == Config.RES_BPJS){
            imagecros.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            setToolbar(Config.BPJS);
        }else if (type == Config.RES_PLN){
            imagecros.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            setToolbar("PLN PRABAYAR");
        }else if(type == Config.RES_DEPOSIFIRST){
            imagecros.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            setToolbar("PILIH BANK");
        }
        //List<String> StringMenu = Arrays.asList(getResources().getStringArray(R.array.pln_menu));
        //menuPLN = new ArrayList<>(StringMenu);
        adapter = new MenuPopUpAdapter(this,R.layout.list_pop_up_menu,menuPLN,this);
        listMenu.setAdapter(adapter);
        //listMenu.setOnItemClickListener(this);
        edit_search_popupmenu = (EditText)findViewById(R.id.edit_search_popupmenu);
        edit_search_popupmenu.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String src = charSequence.toString().toLowerCase(Locale.getDefault());
                if(src.toString().length()!=0){
                    adapter.filter(src);
                }else{
                    adapter.filter("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    public void setToolbar(String title){
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("list",menuPLN.get(i).getNameMenu()+"");
        Intent returnIntent = new Intent();
        //MenuObj obj = new MenuObj(menuPLN.get(i).getNameMenu(),menuPLN.get(i).getCode());
        returnIntent.putExtra("name", name);
        returnIntent.putExtra("code", code);
        setResult(Activity.RESULT_OK,returnIntent);
        /*if(type == Config.RES_DEPOSIFIRST) {
            setResult(Config.RES_DEPOSIFIRST, returnIntent);
        }else if(type== Config.RES_KOMPLAIN){
            setResult(Config.RES_KOMPLAIN,returnIntent);
        }else if(type == Config.RES_PROPINSI){
            setResult(Config.RES_PROPINSI,returnIntent);
        }else if(type == Config.RES_KOTA){
            setResult(Config.RES_KOTA,returnIntent);
        }else if(type == Config.RES_LOKET){
            setResult(Config.RES_LOKET,returnIntent);
        }*/

    }

    @Override
    public void onClickedList(MenuObj obj) {
        Log.d("list",obj.getNameMenu()+"tekkek");
        name = obj.getNameMenu();
        code = obj.getCode();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("name", name);
        returnIntent.putExtra("code", code);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
    public void selectItem(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("name", name);
        returnIntent.putExtra("code", code);
        setResult(Activity.RESULT_OK,returnIntent);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.icon_cross){
            edit_search_popupmenu.setText("");
        }
    }
}
