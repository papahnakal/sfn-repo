package com.fastpay.app.fastpay.request;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.PreferenceClass;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by skyshi on 19/01/17.
 */

public class StringJson {
    private static final String TAG = "jsonString";
    Context contexts;
    PreferenceClass preferenceClass;
    String uuid;
    public StringJson(Context context){
        this.contexts = context;
        preferenceClass = new PreferenceClass(contexts);
        TelephonyManager tManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        uuid = tManager.getDeviceId();
    }
    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }
    public static String getDeviceinfo(){
        String device_information="";
        device_information += "OS Version: " +
                System.getProperty("os.version") + "(" +
                android.os.Build.VERSION.INCREMENTAL + ")";
        device_information += "OS API Level: " +
                android.os.Build.VERSION.SDK_INT;
        device_information += "Device: " +
                android.os.Build.DEVICE;
        device_information += "Model (and Product): " +
                android.os.Build.MODEL+"("+ Build.PRODUCT+")";
        device_information += "RELEASE: " +
                android.os.Build.VERSION.RELEASE;
        device_information += "BRAND: " +
                android.os.Build.BRAND;
        device_information += "DISPLAY: " +
                android.os.Build.DISPLAY;
        device_information += "CPU_ABI: " +
                android.os.Build.CPU_ABI;
        device_information += "CPU_ABI2: " +
                android.os.Build.CPU_ABI2;
        device_information += "UNKNOWN: " +
                android.os.Build.UNKNOWN;
        device_information += "HARDWARE: " +
                android.os.Build.HARDWARE;
        device_information += "Build ID: " +
                android.os.Build.ID;
        device_information += "MANUFACTURER: " +
                android.os.Build.MANUFACTURER;
        device_information += "SERIAL: " +
                android.os.Build.SERIAL;
        device_information += "USER: " +
                android.os.Build.USER;
        device_information += "HOST: " +
                Build.HOST;
        Log.d(TAG,device_information);
        return device_information;
    }
    public String signOn(String id,String pin,String key){
        return "{\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"signon\","
                +"\"credential_data\" : {"
                    +"\"id_outlet\" : \""+id+"\","
                    +"\"pin\" : \""+pin+"\","
                    +"\"key_validation\" : \""+key+"\""
                +"},"

        +"\"additional_data\" : {"
                    +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                    +"\"uuid\":\""+uuid+"\","
                    +"\"app_id\":\"MobileSBF\","
                    +"\"device_information\" : \""+getDeviceinfo()+"\","
                    +"\"version\":\"2.0.0\""
                    +"}"
        +"}";

    }
    public String requestKey(String id){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"request_key\","

                +"\"credential_data\" : {"
                    +"\"id_outlet\"     : \""+id+"\""
                +"},"


                +"\"additional_data\" : {"
                    +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                    +"\"uuid\":\""+uuid+"\","
                    +"\"app_id\":\"MobileSBF\","
                    +"\"device_information\" : \""+getDeviceinfo()+"\","
                    +"\"version\":\"2.0.0\""
                    +"}"
                +"}";
    }
    public String requestListProduct(String typeProduct){
        return "{"
                +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_produk\","

                +"\"includes\" : {"
                +"\"main_product_code\" : \""+typeProduct+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestInquiryTvKabel(String code,String id){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"nomor_pelanggan\" : \""+id+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentTvKabel(String product_code,
                                        String id_pelanggan,
                                        String reff_id_inq,
                                        String nominal_inq,
                                        String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_pelanggan\" : \""+id_pelanggan+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*TELPPASCA*/
    public String requestInquiryTelpPasca(String code,String id){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"nomor_handphone\" : \""+id+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentTelpPasca(String product_code,
                                          String id_pelanggan,
                                          String reff_id_inq,
                                          String nominal_inq,
                                          String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_handphone\" : \""+id_pelanggan+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*KREDIT*/
    public String requestInquiryKredit(String code,String id, String setoran){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"nomor_kartu_kredit\" : \""+id+"\","
                +"\"jumlah_setoran\" : \""+setoran+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentKredit(String product_code,
                                       String id_pelanggan,
                                       String nominal,
                                       String reff_id_inq,
                                       String nominal_inq,
                                       String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_kartu_kredit\" : \""+id_pelanggan+"\","
                +"\"jumlah_setoran\" : \""+nominal+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*CICILAN*/
    public String requestInquiryCicilan(String code,String id, String setoran){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"nomor_kontrak\" : \""+id+"\","
                +"\"jumlah_setoran\" : \""+setoran+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentCicilan(String product_code,
                                        String id_pelanggan,
                                        String nominal,
                                        String reff_id_inq,
                                        String nominal_inq,
                                        String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_kontrak\" : \""+id_pelanggan+"\","
                +"\"jumlah_setoran\" : \""+nominal+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*GAME*/
    public String requestProviderGame(){
        return "{"
                +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_provider_game\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestProductGame(String code){
        return "{"
                +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_produk_game\","

                +"\"includes\" : {"
                +"\"provider_code\" : \""+code+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentGame(String code,String notel){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"nomor_handphone\" : \""+notel+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";

    }
    /*PULSA*/
    public String requestPulsaList(String notel){
        return "{"
                +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_produk_pulsa\","

                +"\"includes\" : {"
                +"\"nomor_handphone\" : \""+notel+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPulsaPayment(String code,String notel){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"nomor_handphone\" : \""+notel+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*PDAM*/
    public String requestInquiryPDAM(String code,String id,String nometer){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"kode_pelanggan\" : \""+id+"\","
                +"\"nomor_sambungan\" : \""+nometer+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentPDAM(String product_code,
                                     String id_pelanggan,
                                     String nometer,
                                     String reff_id_inq,
                                     String nominal_inq,
                                     String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"kode_pelanggan\" : \""+id_pelanggan+"\","
                +"\"nomor_sambungan\" : \""+nometer+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*TELKOM*/
    public String requestTeleponInquiry(String kodeArea,String noTelepon){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+ Config.TELKOM_TELEPON+"\","
                +"\"kode_area\" : \""+kodeArea+"\","
                +"\"no_telp\" : \""+noTelepon+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentTelkom(String product_code,
                                     String kode_area,
                                     String no_telepon,
                                     String reff_id_inq,
                                     String nominal_inq,
                                     String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"kode_area\" : \""+kode_area+"\","
                +"\"no_telp\" : \""+no_telepon+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestSpeedyInquiry(String no_speedy){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+ Config.TELKOM_SPEEDY+"\","
                +"\"nomor_speedy\" : \""+no_speedy+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentSpeedy(String product_code,
                                       String nomor_speedy,
                                       String reff_id_inq,
                                       String nominal_inq,
                                       String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_speedy\" : \""+nomor_speedy+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestCarousel(){
        return "{"
            +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_slide\","

                +"\"credential_data\" : {"
                    +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                    +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                    +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"


                +"\"additional_data\" : {"
                    +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                    +"\"uuid\":\""+uuid+"\","
                    +"\"app_id\":\"MobileSBF\","
                    +"\"version\":\"2.0.0\""
                    +"}"
                +"}";
    }
    public String requestPaymentPLN(String product_code,String id_pelanggan,String id_inq,String nominal_inq,String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"id_pelanggan\" : \""+id_pelanggan+"\","
                +"\"reff_id_inq\" : \""+id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentPLNNontalgis(String product_code,String id_pelanggan,String id_inq,String nominal_inq,String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_registrasi\" : \""+id_pelanggan+"\","
                +"\"reff_id_inq\" : \""+id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentPLNPrabayar(String product_code,String id_pelanggan,String id_inq,String nominal_inq,String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"id_pelanggan\" : \""+id_pelanggan+"\","
                +"\"reff_id_inq\" : \""+id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"denom\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*PLN*/
    public String requestPLNpascabayar(String id_pelanggan){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                    +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                    +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                    +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                    +"\"product_code\" : \"PLNPASC\","
                    +"\"id_pelanggan\" : \""+id_pelanggan+"\""
                    +"},"

                +"\"additional_data\" : {"
                    +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                    +"\"uuid\":\""+uuid+"\","
                    +"\"app_id\":\"MobileSBF\","
                    +"\"version\":\"2.0.0\""
                    +"}"
                +"}";


    }
    public String requestPLNPrabayar(String id_pelanggan,String denom){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \"PLNPRA\","
                +"\"id_pelanggan\" : \""+id_pelanggan+"\","
                +"\"denom\" : \""+denom+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPLNNontalgis(String id_pelanggan){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \"PLNNON\","
                +"\"nomor_registrasi\" : \""+id_pelanggan+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestTokenPrice(){
        return "{"
                +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_denom_plnpra\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*ADMIN*/
    public String requestOperatorSel(){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"list_opsel\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestHargaPulsa(String opsel_code){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"harga_pulsa\","

                +"\"includes\" : {"
                +"\"opsel_code\" : \""+opsel_code+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestKomisi(String password){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"rekap_komisi\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+password+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestInfo(String info){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"info\","

                +"\"includes\" : {"
                +"\"opsel_code\" : \""+info+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPropinsi(){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"list_propinsi\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestKota(String prop_code){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"list_kota\","

                +"\"includes\" : {"
                +"\"prop_code\" : \""+prop_code+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestLocket(){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"list_tipe_loket\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPendaftaran(String no_Hape,String nama,String alamat,String prop_code,String city_code,String kode_pos,String type_loket){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"agensi\","

                +"\"includes\" : {"
                +"\"no_hp\" : \""+no_Hape+"\","
                +"\"nama\" : \""+nama+"\","
                +"\"alamat\" : \""+alamat+"\","
                +"\"prop_code\" : \""+prop_code+"\","
                +"\"city_code\" : \""+city_code+"\","
                +"\"kode_pos\" : \""+kode_pos+"\","
                +"\"tipe_loket\" : \""+type_loket+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestTransfer(String id_tujuan,String transfer,String pin){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"transfer\","

                +"\"includes\" : {"
                +"\"id_outlet_tujuan\" : \""+id_tujuan+"\","
                +"\"nilai_transfer\" : \""+transfer+"\""
                +"},"

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+pin+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestTypeKomplain(){
        return "{"
                +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_kategori_komplain\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestBank(){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"list_bank\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestDeposit(String code_bank, String amount){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"deposit\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"bank_code\" : \""+code_bank+"\","
                +"\"deposit_amount\" : \""+amount+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*ASURANSI*/
    public String requestInquiryAsuransi(String nomor_polis,String code){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+code+"\","
                +"\"nomor_polis\" : \""+nomor_polis+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentAsuransi(String product_code,
                                         String nomor_polis,
                                         String reff_id_inq,
                                         String nominal_inq,
                                         String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_polis\" : \""+nomor_polis+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    /*BPJS*/
    public String requestBulanPremiBPJS(){
        return "{"
                +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_bulan_premi_bpjs\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestInquiryBPJSKS(String nomor_va,String month_value){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"inquiry\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \"ASRBPJSKS\","
                +"\"nomor_va\" : \""+nomor_va+"\","
                +"\"month_value\" : \""+month_value+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestPaymentBPJSKES(String product_code,
                                       String nomor_va,
                                       String month_value,
                                       String reff_id_inq,
                                       String nominal_inq,
                                       String admin_inq){
        return "{"
                +"\"msg_type\" : \"transaction\","
                +"\"processing_code\" : \"payment\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"product_code\" : \""+product_code+"\","
                +"\"nomor_va\" : \""+nomor_va+"\","
                +"\"month_value\" : \""+month_value+"\","
                +"\"reff_id_inq\" : \""+reff_id_inq+"\","
                +"\"nominal_inq\" : \""+nominal_inq+"\","
                +"\"nominal_admin_inq\" : \""+admin_inq+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestCekSaldo(){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"saldo\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"


                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\"20170105163821\","
                +"\"uuid\":\"0000FFF0-0000-1000-8000-00805F9B34FB\","
                +"\"app_id\":\"MobileSBF\","
                +"\"device_information\" : \"AndroidKitKat111\","
                +"\"version\":\"1.0.0\""
                +"}"
                +"}";
    }
    /*laporan transaksi*/
    public String requestLaporanTransaksi(String start_date,String end_date,String no_pelanggan){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"data_transaksi\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"start_date\" : \""+start_date+"\","
                +"\"end_date\" : \""+end_date+"\","
                +"\"offset\" : \""+0+"\","
                +"\"limit\" : \""+100+"\","
                +"\"id_pelanggan\" : \""+no_pelanggan+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestCetakTransaksi(String id_transaksi){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"cetak_ulang\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"

                +"\"includes\" : {"
                +"\"id_transaksi\" : \""+id_transaksi+"\""
                +"},"

                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\""+getCurrentTimeStamp()+"\","
                +"\"uuid\":\""+uuid+"\","
                +"\"app_id\":\"MobileSBF\","
                +"\"version\":\"2.0.0\""
                +"}"
                +"}";
    }
    public String requestUbahPin(){
        return "{"
                +"\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"ganti_pin\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"


                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\"20170105163821\","
                +"\"uuid\":\"0000FFF0-0000-1000-8000-00805F9B34FB\","
                +"\"app_id\":\"MobileSBF\","
                +"\"device_information\" : \"AndroidKitKat111\","
                +"\"version\":\"1.0.0\""
                +"}"
                +"}";
    }
}
